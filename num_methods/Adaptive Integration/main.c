#include"adap.h"

int main(){
	
	int calls = 0;
	double eps = 1e-6, acc = 1e-6;

	// Define functions
	double f1(double x){
		calls++;
		return sqrt(x);
	}

	double f2(double x){
		calls++;
		return 1.0/sqrt(x);
	}

	double f3(double x){
		calls++;
		return log(x)/sqrt(x);
	}

	double f4(double x){
		calls++;
		return 4*sqrt(1-(1.0-x)*(1.0-x));
	}

	double gauss(double x){
		calls++;
		return exp(-x*x/2);
	}

	double gauss_gsl(double x, void* p){
		calls++;
		return exp(-x*x/2);
	}
	
	//_____TASK 1_____
	printf("_____TASK 1_____\n\n");
	
	printf("The following integrals is done with acc = %lg and eps = %lg.\n\n",acc,eps);

	// Sqrt(x) from 0 to 1
	double result = adapt(f1,0,1,acc,eps);
	printf("sqrt(x) integrated from 0 to 1 is equal to %.20lg in %i calls\n",result,calls);
	
	// 1/sqrt(x) from 0 to 1
	calls = 0;
        result = adapt(f2,0,1,acc,eps);
	printf("1/sqrt(x) integrated from 0 to 1 is equal to %lg in %i calls\n",result,calls);

	// log(x)/sqrt(x) from 0 to 1
	calls = 0;
        result = adapt(f3,0,1,acc,eps);
	printf("ln(x)/sqrt(x) integrated from 0 to 1 is equal to %lg in %i calls\n",result,calls);

	//4*sqrt(1-(1-x)^2 from 0 to 1
        calls = 0;
        result = adapt(f4,0,1,acc,eps);
        printf("4*sqrt(1-(1-x)^2) integrated from 0 to 1 is equal to %.20lg in %i calls\n",result,calls);

	//_____TASK 2_____
	printf("\n____TASK 2_____\n\n");		
	
	// Gauss in different intervals
	printf("Gaussian function integrated from:\n");
	
	calls=0;
	result = adapt(gauss,-INFINITY,INFINITY,acc,eps);
	printf("-inf to inf is equal to %.20lg in %i calls\n",result,calls);
	
        calls=0;
        result = adapt(gauss,-INFINITY,0,acc,eps);
        printf("-inf to 0 is equal to %.20lg in %i calls\n",result,calls);

        calls=0;
        result = adapt(gauss,0,INFINITY,acc,eps);
        printf("0 to inf is equal to %.20lg in %i calls\n",result,calls);
	
	// GSL integration
	gsl_function F;
	F.function = &gauss_gsl;
	F.params = NULL;

	int limit = 1e6;
	gsl_integration_workspace* ws = gsl_integration_workspace_alloc(limit);
	double err;
	int flag = gsl_integration_qagi(&F,eps,acc,limit,ws,&result,&err);

	printf("The full integral of this even function is %lg with the GSL routine, so the integrator works just fine.\n",result);

	//_____TASK 3_____
	printf("\n_____TASK 3_____\n\n");
	
	printf("THe following integrals is calculated with the Clenshaw-Curtis transformation with eps = %lg and acc = %lg\n\n",eps,acc);

	// Clenshaw-Curtis routine on the previously defined functions with divergiencies
	calls = 0;
        result = clenshaw_curtis(f2,0,1,acc,eps);
        printf("1/sqrt(x) integrated from 0 to 1 is equal to %lg in %i calls\n",result,calls);

	calls = 0;
        result = clenshaw_curtis(f4,0,1,acc,eps);
        printf("4*sqrt(1-(1-x)^2) integrated from 0 to 1 is equal to %lg in %i calls\n",result,calls);

	printf("The Clenshaw-Curtis transformations reduces the number of function calls significantly compared to the previous integrations when divergencies are present, but not otherwise.\n");
	gsl_integration_workspace_free(ws);
	return 0;
}
