#include<assert.h>
double linterp(int n, double* x, double* y, double z){
	assert(n>1 && z>=x[0] && z<=x[n-1]);
	int i=0, j=n-1;
	while(j-i>1){
		int m = (i+j)/2;
		if(z>x[m])
			i=m;
		else
			j=m;
		}
	return y[i]+(y[i+1]-y[i])/(x[i+1]-x[i])*(z-x[i]);
}

double linterp_integ(int n, double* x, double* y, double z){
	assert(n>1 && z>=x[0] && z<=x[n-1]);
	int i=0, j=n-1;
	while(j-i>1){
		int m = (i+j)/2;
		if(z>x[m])
			i=m;
		else
			j=m;
		}
        double integral=0.0;
	int k = 0;
	while(k<i){
		integral += 1.0/2*(y[k+1]-y[k])/(x[k+1]-x[k])*(x[k+1]-
		                     x[k])*(x[k+1]-x[k])+y[k]*(x[k+1]-x[k]);
		k++;
	}
	
	integral += 1.0/2*(y[i+1]-y[i])/(x[i+1]-x[i])*(z-
	                 x[i])*(z-x[i])+y[i]*(z-x[i]);
	return integral;
}

