#include<stdio.h>
#include<stdlib.h>
#include"splines.h"
#include<gsl/gsl_spline.h>
#include<gsl/gsl_errno.h>

int main(int argc, char** argv){
	/* ____Data is loaded from file____*/
	int n=0;
	if(argc>1) n=atoi(argv[1])-1;
	
	double x[n], y_1[n], y_2[n];
	for(int i=0;i<n;i++){
		scanf("%lg %lg %*s %lg %*s",&x[i],&y_1[i],&y_2[i]);
	}

	/* ____Linear interpolation is made along with integration____*/
	for(double i=x[0];i<=x[n-1];i+=0.01)
		printf("%lg %lg %lg %lg %lg\n"
				,i,linterp(n,x,y_1,i),linterp_integ(n,x,y_1,i)
				, linterp(n,x,y_2,i),linterp_integ(n,x,y_2,i));
	printf("\n\n");

	/* ____Quadratic interpolation is made____ */
	quad_spline* S = quad_spline_alloc(n,x,y_1);
	
	for(double i=x[0];i<=x[n-1];i+=0.01){
		printf("%lg %lg %lg %lg\n",i,quad_spline_eval(S,i)
				,quad_spline_integ(S,i),quad_spline_deriv(S,i));
	}
	printf("\n\n");

	quad_spline_free(S);

	/* ____Cubic interpolation is made____*/
	cubic_spline* C = cubic_spline_alloc(n,x,y_1);
	
	/* GSL cubiv spline */
	gsl_interp_accel *accel_ptr = gsl_interp_accel_alloc();
	gsl_spline *spline_ptr = gsl_spline_alloc(gsl_interp_cspline,n);
	gsl_spline_init(spline_ptr,x,y_1,n);
		
	for(double i=x[0];i<=x[n-1];i+=0.01){
		printf("%lg %lg %lg %lg %lg\n"
				,i,cubic_spline_eval(C,i)
				,cubic_spline_integ(C,i)
				,cubic_spline_deriv(C,i)
				,gsl_spline_eval(spline_ptr,i,accel_ptr));
	}
	cubic_spline_free(C);
	gsl_spline_free(spline_ptr);
	gsl_interp_accel_free(accel_ptr);

	return 0;
}
