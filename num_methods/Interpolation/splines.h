#ifndef HAVE_SPLINES_H

typedef struct {int n; double *x, *y, *b, *c;} quad_spline;

quad_spline* quad_spline_alloc(int n, double* x, double* y);

typedef struct {int n; double *x, *y, *b, *c, *d;} cubic_spline;

cubic_spline* cubic_spline_alloc(int n, double* x, double* y);

double linterp(int n, double* x, double* y, double z);
double linterp_integ(int n, double* x, double* y, double z);
double quad_spline_eval(quad_spline* s, double z);
double quad_spline_integ(quad_spline* s, double z);
double quad_spline_deriv(quad_spline* s, double z);
double cubic_spline_eval(cubic_spline* s, double z);
double cubic_spline_integ(cubic_spline* s, double z);
double cubic_spline_deriv(cubic_spline* s, double z);
void quad_spline_free(quad_spline *s);
void cubic_spline_free(cubic_spline* s);


#define HAVE_SPLINES_H
#endif
