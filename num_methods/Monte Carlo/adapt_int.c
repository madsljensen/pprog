#include"monte.h"

double adapt24(double f(double), double a, double b, double acc, double eps,
		double f2, double f3, int nrec){
	assert(nrec<1e6);
	double f1 = f(a+(b-a)/6), f4 = f(a+5*(b-a)/6);
	double Q = (2*f1+f2+f3+2*f4)/6*(b-a);
	double q = (f1+f2+f3+f4)/4*(b-a);
	double tol = acc+eps*fabs(Q);
	double err = fabs(Q-q)/2;

	if(err<tol)
		return Q;
	// Recursive calls
	else{
		double Q1 = adapt24(f,a,(a+b)/2,acc/sqrt(2.0),eps,f1,f2,nrec+1);
		double Q2 = adapt24(f,(a+b)/2,b,acc/sqrt(2.0),eps,f3,f4,nrec+1);
		return Q1+Q2;
	}
}

double adapt(double f(double), double a, double b, double acc, double eps){
	double f2, f3, a_or = a, b_or = b;

	double f_corr(double x){
		if(isinf(a_or)==-1 && isinf(b_or)==1){	
			return (f((1-x)/x)+f((x-1)/x))*1.0/(x*x);
			}
					
		else if(isinf(a_or)!=-1 && isinf(b_or)==1){
			return (f(a_or+(1-x)/x))*1.0/(x*x);
			}
			
		else if(isinf(a_or)==-1 && isinf(b_or)!=1){
			return (f(b_or-(1-x)/x))*1.0/(x*x);
			}
		else return 0;
	}
	
	int nrec = 0;

	if(isinf(a_or)==-1 || isinf(b_or)==1){
		a = 0; b = 1;
		f2 = f_corr(a+2*(b-a)/6); f3 = f_corr(a+4*(b-a)/6);
		return adapt24(f_corr,a,b,acc,eps,f2,f3,nrec);
	}
	else{
		f2 = f(a+2*(b-a)/6); f3 = f(a+4*(b-a)/6);
		return adapt24(f,a,b,acc,eps,f2,f3,nrec);
	}
}

double clenshaw_curtis(double f(double), double a, double b, double acc, double eps){
	double a_or = a, b_or = b;

	double f_cc(double t){
		double x = (a_or+b_or)/2+(a_or-b_or)/2*cos(t);
		return f(x)*sin(t)*(b_or-a_or)/2;
	}
	a = 0; b = M_PI;
	int nrec = 0;

	double f2 = f_cc(a+2*(b-a)/6), f3 = f_cc(a+4*(b-a)/6);
	return adapt24(f_cc,a,b,acc,eps,f2,f3,nrec);
}

double area_adapt(double f(double x, double y),double c(double), double d(double)
		, double a, double b, double acc, double eps){
	double a_or = a, b_or = b;
	
	double f_out(double x){
		double f_in(double y){
			return f(x,y);
		}
		a = c(x);
		b = d(x);

		return adapt(f_in,a,b,acc,eps);
	}


	return adapt(f_out,a_or,b_or,acc,eps);
}
