#include"monte.h"

int main(){
	
	//Allocate space
	int dim = 3, N = 1e6;
	gsl_vector* a = gsl_vector_alloc(dim);
	gsl_vector* b = gsl_vector_alloc(dim);

	double result, error;
	
	//_____TASK 1_____
	printf("_____TASK 1_____\n\n");

	// Test of Spherical integration (three dimension)
	gsl_vector_set_zero(a);
	gsl_vector_set(b,0,1);
	gsl_vector_set(b,1,2*M_PI);
	gsl_vector_set(b,2,M_PI);

	double f1(gsl_vector* x){
		double x_0 = gsl_vector_get(x,0);
		return x_0*x_0;
	}

	plainmc(a,b,f1,N,&result,&error);
	
	printf("Integrating dr d_phi d_theta r^2 from 0 to 1, 0 to 2*pi and 0 to pi with %i points, resulting in %lg with an error of %lg.\n",N,result,error);
	
	// Spherical volume integral
	gsl_vector_set_zero(a);
	gsl_vector_set(b,0,1);
	gsl_vector_set(b,1,2*M_PI);
	gsl_vector_set(b,2,M_PI);

	double f2(gsl_vector* x){
		double x_0 = gsl_vector_get(x,0);
		double z_0 = gsl_vector_get(x,2);
		return x_0*x_0*sin(z_0);
	}
	
	plainmc(a,b,f2,N,&result,&error);

	printf("Integrating dr d_phi d_theta r^2*sin(theta) from 0 to 1, 0 to 2*pi and 0 to pi with %i points, resulting in %lg with an error of %lg.\n",N,result,error);
	
	// Integral given in exercise
	N = 1e6;
	gsl_vector_set_zero(a);
	gsl_vector_set(b,0,M_PI);
	gsl_vector_set(b,1,M_PI);
	gsl_vector_set(b,2,M_PI);	
	
	double f3(gsl_vector* x){
		double x_0 = gsl_vector_get(x,0),
		       y_0 = gsl_vector_get(x,1),
		       z_0 = gsl_vector_get(x,2);
		return 1.0/(M_PI*M_PI*M_PI)*1.0/(1.0-cos(x_0)*cos(y_0)*cos(z_0));
	}

	plainmc(a,b,f3,N,&result,&error);

        printf("Integrating dx dx dz 1/pi^3*1/(1-cos(x)cos(y)cos(z) from 0 to pi, 0 to pi and 0 to pi with %i points, resulting in %lg with an error of %lg.\n",N,result,error);

	//_____TASK 2_____
	printf("\n_____TASK 2_____\n\n");
	
	int max = 0.5e3, step = 2;

	FILE* data = fopen("data.txt","w+");
	for(int i = 5;i<max;i+=step){
		gsl_vector_set_zero(a);
	        gsl_vector_set(b,0,1);
	        gsl_vector_set(b,1,2*M_PI);
	        gsl_vector_set(b,2,M_PI);
		
		plainmc(a,b,f1,i,&result,&error);

		fprintf(data,"%i\t%lg\n",i,error);
	}
	fclose(data);
	printf("The error vs number of points is investigated for the second spherical integral (the volume integral) and as seen in 'plot.svg' the behaviour is indeed of type 1/sqrt(x). The coefficient is found to be 5.3.\n\n");

	//_____TASK 3____
	printf("_____TASK 3_____\n\n");
	double eps = 1e-6, acc = 1e-6;

	printf("The following integrals is calculated with eps = %lg and acc = %lg.\n\n",eps,acc);	
	// Define functions
	double c(double x){
		return cos(x);
	}

	double d(double x){
		return sin(x);
	}
	
	double f4(double x,double y){
		return sin(x)*cos(y);
	}

	
	double c1(double x){
		return x;
	}

	double d1(double x){
		return x*x*x;
	}

	double f5(double x,double y){
		return x*y*y;
	}	
	
	// Preform integrals
	result = area_adapt(f4,c,d,0,M_PI,acc,eps);

	printf("Integral dx dy sin(x)*cos(x) from 0 to pi and cos(x) to sin(x) results in %lg compared to the theoretically expected 1.38246.\n",result);

	result = area_adapt(f5,c1,d1,-1,1,acc,eps);

        printf("Integral dx dy x*y^2 from -1 to 1 and x to x^3  results in %lg compared to the theoretically expected -0.0727273\n",result);

	
	// Free space
	gsl_vector_free(a);
	gsl_vector_free(b);
	return 0;
}
