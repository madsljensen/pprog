#ifndef HAVE_MONTE_H
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<assert.h>
#include<math.h>

void vector_print(const gsl_vector* V);
void matrix_print(const gsl_matrix* M);
void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);
void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B);

void plainmc(gsl_vector* a, gsl_vector* b, double f(gsl_vector* x), int N,
		                double* result, double* error);
double area_adapt(double f(double x, double y),double c(double), double d(double)
		                , double a, double b, double acc, double eps);

#define HAVE_MONTE_h
#endif
