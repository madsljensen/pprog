#include"monte.h"
#define RND ((double)rand()/RAND_MAX)

void random_x(gsl_vector* a, gsl_vector* b, gsl_vector* x){
	int n = x->size;
	assert(a->size == n && b->size == n);

	for(int i =0;i<n;i++){
		double a_i = gsl_vector_get(a,i),
		       b_i = gsl_vector_get(b,i);

		gsl_vector_set(x,i,a_i+RND*(b_i-a_i));
	}
}

void plainmc(gsl_vector* a, gsl_vector* b, double f(gsl_vector* x), int N,
		double* result, double* error){
	int dim = a->size;

	double V = 1;
	for(int i=0;i<dim;i++){
		V*=gsl_vector_get(b,i)-gsl_vector_get(a,i);
	}
	double sum =0, sum2 = 0, fx;
	gsl_vector* x = gsl_vector_alloc(dim);
	

	for(int i=0;i<N;i++){
		random_x(a,b,x);
		fx = f(x);
		sum+=fx;
		sum2+=fx*fx;
	}
	
	double avr = sum/N, var = sum2/N-avr*avr;
	*result = avr*V;
	*error = sqrt(var/N)*V;

	gsl_vector_free(x);
}
				
