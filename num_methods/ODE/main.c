#include"hode.h"

int main(){
	
	// Set parameters ans allocate spcae	
	int n=2, k, n_max = 1e8;
	double b=M_PI/2, h=b/20, acc=1e-12, eps=1e-6;

	gsl_vector* xlist = gsl_vector_alloc(n_max+1);
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_matrix* ylist = gsl_matrix_alloc(n_max+1,n);

	// define functions
	void sine_diff_eq(int n, double x, gsl_vector* y, gsl_vector* dydx){
		double y0 = gsl_vector_get(y,0);
		double y1 = gsl_vector_get(y,1);
		gsl_vector_set(dydx,0,y1);
		gsl_vector_set(dydx,1,-y0);
	}

	void orbit(int n, double x, gsl_vector* y, gsl_vector* dydx){
		double y0 = gsl_vector_get(y,0);
		double y1 = gsl_vector_get(y,1);
		gsl_vector_set(dydx,0,y1);
		gsl_vector_set(dydx,1,1-y0+y0*y0);
	}

	double int1(double x){
		return x;	
	}

	double int2(double x){
		return exp(-x*x/2);
	}
		
	//_____TASK 1+2_____
	printf("_____ TASK 1+2 (rk21 Euler midpoint stepper)_____\n");
	
	// Test of ODE solver on sine function
	gsl_vector_set(xlist,0,0.0);
	gsl_matrix_set(ylist,0,0,1.0);
	gsl_matrix_set(ylist,0,1,0.0);

	printf("Testing on trigonometric system with dy/dx_00=y1 and dy/dx_1=-y0 (sine function)\n");
	printf("eps = %lg, acc = %lg,  max = %i, b = %lg, hstart = %g\n",
			eps, acc,n_max,b,h);
	printf("Start conditions are set to (for x = %lg)\n",gsl_vector_get(xlist,0));
	gsl_matrix_get_row(y,ylist,0);
	vector_print(y);
	
	k = ode_driver(sine_diff_eq,n,xlist,ylist,b,h,acc,eps,n_max);
	
	printf("The solution at y(b) is\n");
	gsl_matrix_get_row(y,ylist,k-1);
	vector_print(y);
	printf("reached in %i steps.\n\n",k);
	

	// Orbit test
        gsl_vector_set(xlist,0,0.0);
	gsl_matrix_set(ylist,0,0,0.0);
	gsl_matrix_set(ylist,0,1,-0.5);


        b = M_PI/2; h=1e-3;
	printf("Testing on orbit system from PP with dy/dx_0=y1 and dy/dx_1=1-y0+y0^2 (epsilon=1)\n");
        printf("eps = %lg, acc = %lg,  max = %i, b = %lg, hstart = %g\n",
                  eps, acc,n_max,b,h);
        printf("Start conditions are set to (for x = %lg)\n",gsl_vector_get(xlist,0));
        
	gsl_matrix_get_row(y,ylist,0);
        vector_print(y);
        
	k = ode_driver(orbit,n,xlist,ylist,b,h,acc,eps,n_max);
        
	printf("The solution at y(b) is\n");
        gsl_matrix_get_row(y,ylist,k-1);
        vector_print(y);
        printf("reached in %i steps.\n\n",k);

	//_____TASK 3_____
	printf("_____TASK 3_____\n");
	n=1;

	// Integrate x from -a to a
	acc = 1e-6; eps = 1e-9; b = 1000 ; h=1e-3;
	gsl_vector_set(xlist,0,-1000);
	gsl_matrix* ylist_integ = gsl_matrix_alloc(n_max+1,n);
	gsl_matrix_set_zero(ylist_integ);	
	printf("A good start-test is an ueven function such as f(x) = x integrated from -a to a as this should be zero\n");
	printf("eps = %lg, acc = %lg,  max = %i, b = %lg, hstart = %g\n",
			                  eps, acc,n_max,b,h);
	printf("Start conditions are set to (for x = %lg)\n",gsl_vector_get(xlist,0));
       	printf("%lg\n",gsl_matrix_get(ylist_integ,0,0));

	double integral  = ode_integrate(int1,n,xlist,ylist_integ,b,h,acc,eps,n_max);
	printf("Integral = %lg\n\n",integral);
	
	// Integrale Gauss from -inf to inf
        acc = 1e-6; eps = 1e-6; b = 1e9 ; h=1e-3;
        gsl_vector_set(xlist,0,0);
        gsl_matrix_set_zero(ylist_integ);
        
	printf("Next a Gauss function of type exp(-x*x/2) from 0 to 1e9 (~inf) which should be pi/sqrt(2)=1.25311.\n");
        printf("eps = %lg, acc = %lg,  max = %i, b = %lg, hstart = %g\n",
                                         eps, acc,n_max,b,h);
        printf("Start conditions are set to (for x = %lg)\n",gsl_vector_get(xlist,0));
       	printf("%lg\n",gsl_matrix_get(ylist_integ,0,0));

	integral  = ode_integrate(int2,n,xlist,ylist_integ,b,h,acc,eps,n_max);
        printf("Integral = %lg\n",integral);



	// Free space
	gsl_vector_free(xlist);
	gsl_vector_free(y);
	gsl_matrix_free(ylist);
	gsl_matrix_free(ylist_integ);
	
	return 0;
}
