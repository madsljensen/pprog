#ifndef HAVE_hODE_H
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<assert.h>
#include<math.h>

void vector_print(const gsl_vector* V);
void matrix_print(const gsl_matrix* M);

int ode_driver(void f(int n, double x, gsl_vector* y, gsl_vector* dydx),
		                int n, gsl_vector* xlist, gsl_matrix* ylist, double b,
				                double h, double acc, double eps, int n_max);
double ode_integrate(double f(double x), int n, gsl_vector* xlist, gsl_matrix* ylist,
		                double b, double h, double acc, double eps, int n_max);

#define HAVE_hODE_h
#endif
