#include"hode.h"

void rkstep12(void f(int n, double x, gsl_vector* y, gsl_vector* dydx),
		int n, double x, gsl_vector* yx, double h, gsl_vector* yh,
		gsl_vector* dy){
	
	// Allocate Space
	gsl_vector* k0 = gsl_vector_alloc(n);
	gsl_vector* yt = gsl_vector_alloc(n);
	gsl_vector* k12 = gsl_vector_alloc(n);
	
	f(n,x,yx,k0);
	for(int i=0;i<n;i++){
		gsl_vector_set(yt,i,gsl_vector_get(yx,i)+
				gsl_vector_get(k0,i)*h/2.0);
	}
	

	f(n,x+h/2.0,yt,k12);
	for(int i=0;i<n;i++){
		gsl_vector_set(yh,i,gsl_vector_get(yx,i)+gsl_vector_get(k12,i)*h);
	}

	for(int i=0;i<n;i++){
		gsl_vector_set(dy,i,
				(gsl_vector_get(k0,i)-gsl_vector_get(k12,i))*h/2);
	}
	
	//Free space
	gsl_vector_free(k0);
	gsl_vector_free(yt);
	gsl_vector_free(k12);

}

int ode_driver(void f(int n, double x, gsl_vector* y, gsl_vector* dydx),
		int n, gsl_vector* xlist, gsl_matrix* ylist, double b,
		double h, double acc, double eps, int n_max){
	int k=0;
	double x, err, norm_y, tol, a = gsl_vector_get(xlist,0);

	// Allocate space
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector* yh = gsl_vector_alloc(n);
	gsl_vector* dy = gsl_vector_alloc(n);

	while(gsl_vector_get(xlist,k)<b){
		x = gsl_vector_get(xlist,k);
		gsl_matrix_get_row(y,ylist,k);
		if(x+h>b){
			h=b-x;
		}
		rkstep12(f,n,x,y,h,yh,dy);
		
		err = gsl_blas_dnrm2(dy);
		norm_y = gsl_blas_dnrm2(yh);

		tol = (norm_y*eps+acc)*sqrt(h/(b-a));

		if(err<tol){
			k++;
			if(k>n_max-1) return -k;
			gsl_vector_set(xlist,k,x+h);
			for(int i=0;i<n;i++){
				gsl_matrix_set(ylist,k,i,gsl_vector_get(yh,i));
			}
		}
		if(err>0)
			h*=pow(tol/err,0.25)*0.95;
		else
			h*=2.0;
	}
	
	// Free space
	gsl_vector_free(y);
	gsl_vector_free(yh);
	gsl_vector_free(dy);

	return k+1;
}

double ode_integrate(double f(double x), int n, gsl_vector* xlist, gsl_matrix* ylist,
		double b, double h, double acc, double eps, int n_max){
	
	void sys(int n, double x, gsl_vector* y, gsl_vector* dydx){
		double f0=f(x);
		gsl_vector_set(dydx,0,f0);
	}
	int k = ode_driver(sys,n,xlist,ylist,b,h,acc,eps,n_max);
	printf("Number of steps %i\n",k);
	return gsl_matrix_get(ylist,k-2,0);
}
