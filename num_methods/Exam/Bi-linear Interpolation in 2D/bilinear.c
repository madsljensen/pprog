#include"splines.h"

double bilinear(gsl_vector* x, gsl_vector* y, 
		gsl_matrix* F, double px, double py){
	// Define sizes
	int nx = x->size, ny = y->size;
	
	assert(F->size1==nx && F->size2==ny);
	assert(px>=gsl_vector_get(x,0) && px<=gsl_vector_get(x,nx-1)
		       	&& py>=gsl_vector_get(y,0) && py<=gsl_vector_get(y,ny-1));

	// Determine which points the wanted interpolated point lies between 
	int i=1,j=1;

	while(gsl_vector_get(x,i)<px){i++;}
	while(gsl_vector_get(y,j)<py){j++;}
	j--; i--;
	double x0 =gsl_vector_get(x,i), 
	       y0=gsl_vector_get(y,j), 
	       x1=gsl_vector_get(x,i+1),
	       y1=gsl_vector_get(y,j+1), 
	       F00 = gsl_matrix_get(F,i,j), 
	       F11 = gsl_matrix_get(F,i+1,j+1),
	       F10 = gsl_matrix_get(F,i+1,j), 
	       F01 = gsl_matrix_get(F,i,j+1);
		 
	double delta_x = px-x0, delta_y = py-y0, Dx = x1-x0, Dy = y1-y0;
	assert(delta_x>=0 && delta_y>=0);
		
	double c11 = delta_x/Dx*delta_y/Dy;
	double c01 = delta_y/Dy*(1-delta_x/Dx);
	double c10 = delta_x/Dx*(1-delta_y/Dy);
	double c00 = 1-delta_x/Dx-delta_y/Dy+c11;

	return c11*F11+c01*F01+c10*F10+c00*F00;
}

