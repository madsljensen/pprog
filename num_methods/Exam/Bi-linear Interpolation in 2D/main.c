#include"splines.h"

int main(){
	//Allocate space
	int nx = 5, ny=7;
	gsl_vector* x = gsl_vector_alloc(nx);
	gsl_vector* y = gsl_vector_alloc(ny);
	gsl_matrix* F = gsl_matrix_alloc(nx,ny);
	
	gsl_vector* x1 = gsl_vector_alloc(nx);
	gsl_vector* y1 = gsl_vector_alloc(ny);
	gsl_matrix* F1 = gsl_matrix_alloc(nx,ny);

	// Define random rectangular interval scalars
	double rec_x[nx], rec_y[ny], sum_rec_x=0, sum_rec_y=0;
	for(int i=0;i<nx-1;i++){
		rec_x[i]=RND;
		sum_rec_x+=rec_x[i];
	}
	for(int i=0;i<ny-1;i++){
		rec_y[i]=RND;
		sum_rec_y+=rec_y[i];
	}

	//Create data in regular grid and random rectangular grid
	double fun(double x, double y){
		return y*log(y)*exp(-x)-1/(y+x);
	}
	
	double x_min=0.1, x_max = M_PI, y_min = 0.1, y_max = M_PI;
	
	// Regular	
	for(int i=0;i<nx;i++){
		 gsl_vector_set(x,i,x_min+i*(x_max-x_min)/(nx-1));
	}

	for(int i=0;i<ny;i++){
	        gsl_vector_set(y,i,y_min+i*(y_max-y_min)/(ny-1));
	}

	// Rectangular
	double xi=x_min, yi=y_min;

	gsl_vector_set(x1,0,xi);
	for(int i=0;i<nx-1;i++){	
		 xi += rec_x[i]/sum_rec_x*(x_max-x_min);
		 gsl_vector_set(x1,i+1,xi);
	}

	gsl_vector_set(y1,0,yi);
	for(int i=0;i<ny-1;i++){
		yi+=rec_y[i]/sum_rec_y*(y_max-y_min);
		gsl_vector_set(y1,i+1,yi);
	}
	
	// Calculate function values, assign and print them
	for(int i=0;i<nx;i++){
		for(int j=0;j<ny;j++){
			gsl_matrix_set(F,i,j,fun(gsl_vector_get(x,i),gsl_vector_get(y,j)));
			printf("%lg\t%lg\t%lg\n",
				gsl_vector_get(x,i),
				gsl_vector_get(y,j),
				gsl_matrix_get(F,i,j));
		}
	}
	printf("\n\n");
	
	for(int i=0;i<nx;i++){
        	for(int j=0;j<ny;j++){
			gsl_matrix_set(F1,i,j,fun(gsl_vector_get(x1,i),gsl_vector_get(y1,j)));
                        printf("%lg\t%lg\t%lg\n",
                        gsl_vector_get(x1,i),
                        gsl_vector_get(y1,j),
                        gsl_matrix_get(F1,i,j));
                 }
	}
	printf("\n\n");


	//Interpolate
	int interp_nx = 15, interp_ny = 10;

	for(double i = x_min;i<=x_max;i+=(x_max-x_min)/(interp_nx)){
		for(double j = y_min;j<=y_max;j+=(y_max-y_min)/(interp_ny)){
			double interp_F = bilinear(x,y,F,i,j);
			double interp_F1 = bilinear(x1,y1,F1,i,j);
			printf("%lg\t%lg\t%lg\t%lg\n",i,j,interp_F,interp_F1);
		}
	}

	// Free Stuff
	gsl_vector_free(x);
	gsl_vector_free(y);
	gsl_matrix_free(F);
	gsl_vector_free(x1);
	gsl_vector_free(y1);
	gsl_matrix_free(F1);	
	return 0;
}
