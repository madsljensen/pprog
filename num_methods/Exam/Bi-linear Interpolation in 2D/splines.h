#ifndef HAVE_SPLINES_H
#include<assert.h>
#include<math.h>
#include<stdlib.h>
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#define RND (double)rand()/(double)RAND_MAX

double bilinear(gsl_vector* x, gsl_vector* y,                
		                gsl_matrix* F, double px, double py);
#define HAVE_SPLINES_H
#endif
