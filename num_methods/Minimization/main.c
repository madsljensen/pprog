#include"mini.h"

int main(){
	// Define functions
	int ncalls = 0;

	double rosenbrock(gsl_vector* p, gsl_vector* df, gsl_matrix* H){
		ncalls++;
		double X = gsl_vector_get(p,0);
		double Y = gsl_vector_get(p,1);
		double Z = (1-X)*(1-X)+100*(Y-X*X)*(Y-X*X);
		gsl_vector_set(df,0,2.0*X-2.0+400.0*(X*X*X-Y*X));
		gsl_vector_set(df,1,200.0*(Y-X*X));

		gsl_matrix_set(H,0,0,2.0+100.0*(12*X*X-4*Y));
		gsl_matrix_set(H,0,1,-400.0*X);
		gsl_matrix_set(H,1,0,-400*X);
		gsl_matrix_set(H,1,1,200.0);
		return Z;
	}

	double himmelblau(gsl_vector* p, gsl_vector* df, gsl_matrix* H){
	        ncalls++;
        	double X = gsl_vector_get(p,0);
                double Y = gsl_vector_get(p,1);
                double Z = (X*X+Y+11.0)*(X*X+Y-11.0)+(X+Y*Y-7.0)*(X+Y*Y-7.0);
                gsl_vector_set(df,0,4*X*(X*X+Y-11.0)+2.0*(X+Y*Y-7.0));
                gsl_vector_set(df,1,2.0*(X*X+Y-11.0)+4*Y*(X+Y*Y-7.0));
                
		gsl_matrix_set(H,0,0,4*(3*X*X+Y-11)+2);
                gsl_matrix_set(H,0,1,4*(X+Y));
                gsl_matrix_set(H,1,0,4*(X+Y));
                gsl_matrix_set(H,1,1,2+4*(X+3*Y*Y-7));
            return Z;
	}

	double rosenbrock_num(gsl_vector* x){
		ncalls++;
		double X = gsl_vector_get(x,0);
		double Y = gsl_vector_get(x,1);
		double Z = (1-X)*(1-X)+100*(Y-X*X)*(Y-X*X);
		return Z;
	}

        double himmelblau_num(gsl_vector* x){
	        ncalls++;
                double X = gsl_vector_get(x,0);
                double Y = gsl_vector_get(x,1);
                double Z = (X*X+Y-11)*(X*X+Y-11)+(X+Y*Y-7)*(X+Y*Y-7);
                return Z;
        }


	double fit_fun(gsl_vector* v){
		ncalls++;
		double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
		double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
		double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
		int N = sizeof(t)/sizeof(t[0]);

		double A = gsl_vector_get(v,0);
		double B = gsl_vector_get(v,1);
		double T = gsl_vector_get(v,2);

		double fx=0;
		for(int i=0;i<N;i++){
			fx += pow((A*exp(-t[i]/T)+B-y[i])/e[i],2);
		}
		return fx;
	}

	// Allocate space
	gsl_vector* v = gsl_vector_alloc(2);

	// Set tolerance and step size
	double eps = 1e-9, step = 1e-3;
	
//_____TASK 1_____
	printf("_____TASK 1_____\n\n");
	// Rosenbrock minimization
	gsl_vector_set(v,0,-8.0);
	gsl_vector_set(v,1,30.0);
	
	printf("Minimum of Rosenbrock function given the start values\n");
	vector_print(v);

	ncalls=0; 
	int nsteps_ros1 = qnewt(rosenbrock,v,step,eps);
	
	printf("is, with eps = %lg and step size = %lg\n",eps,step);
	vector_print(v);
	printf("with %i steps and %i function calls\n\n",nsteps_ros1,ncalls);
	
	// Himmelblau function
        gsl_vector_set(v,0,30.0);
        gsl_vector_set(v,1,-15.0);
        
	printf("Minimum of Himmelblau function given the start values\n");
        vector_print(v);
        
	ncalls=0;
        int nsteps_him1 = qnewt(himmelblau,v,step,eps);
        
	printf("is, with eps = %lg and step size = %lg\n",eps,step);
        vector_print(v);
        printf("with %i steps and %i function calls\n\n",nsteps_him1,ncalls);

//_____TASK 2_____
	printf("_____TASK 2_____\n\n");

	//Minimum of Rosenbrock  with Broyden and numerical gradient
	step=1e-6;
	gsl_vector_set(v,0,0.0);
        gsl_vector_set(v,1,0.0);
         
	printf("Minimum of Rosenbrock function with Broyden update and numerically calculated gradient given the start values\n");
        vector_print(v);
         
	ncalls=0;
        int nsteps_ros2 = newt_broyden_num_grad(rosenbrock_num,v,step,eps);
        
	printf("is, with eps = %lg and step size = %lg\n",eps,step);
        vector_print(v);
        printf("with %i steps and %i function calls\n\n",nsteps_ros2,ncalls);
	
	
	// Himmelblau with broyden update and numerically calculated gradient
        step = 1e-3;
	gsl_vector_set(v,0,30.0);
        gsl_vector_set(v,1,-15.0);
        
	printf("Minimum of Himmelblau function with Broyden update and numerically calculated gradient given the start values\n");
        vector_print(v);
        
	ncalls=0;
        int nsteps_him2 = newt_broyden_num_grad(himmelblau_num,v,step,eps);
        
	printf("is, with eps = %lg and step size = %lg\n",eps,step);
        vector_print(v);
        printf("with %i steps and %i function calls\n\n",nsteps_him2,ncalls);

	// Comparison
	printf("Comparison:\n");
	printf("Rosenbrock: Steps for analytical %i and for numerical %i\n",nsteps_ros1,nsteps_ros2);
	printf("Himmelblau: Steps for analytical %i and for numerical %i\n",nsteps_him1,nsteps_him2);
	printf("Note: Rosenbrock numerical has tighter start values and a lower step size and still doesen't reach the exact root, which is accomplished by the analytical.\nIn the Root finding excercise, the routine found the roots in 70 and 8 iteratison respectively.\n\n");

	// Fit to the given data
        double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);
		
	// save data in file
	FILE* data = fopen("fit_data.txt","w+");
	for(int i=0;i<N;i++){
		fprintf(data,"%lg\t%lg\t%lg\n",t[i],y[i],e[i]);
	}
	fprintf(data,"\n\n");
	
	// Find constants in minimization
	step=1e-6;
	gsl_vector* x_fit = gsl_vector_alloc(3);
        gsl_vector_set(x_fit,0,0.5);
        gsl_vector_set(x_fit,1,1.0);
        gsl_vector_set(x_fit,2,2.5);
        
	printf("Fit to the given data with Broyden update and numerically calculated gradient given the start values\n");
        vector_print(x_fit);
        
	ncalls=0;
        int nsteps = newt_broyden_num_grad(fit_fun,x_fit,step,eps);
        
	printf("is, with eps = %lg and step size = %lg\n",eps,step);
        vector_print(x_fit);
        printf("with %i steps and %i function calls\n\n",nsteps,ncalls);

	// Print fit
	double t_0=t[0], t_max = t[N-1], delta_t = t_max/200,
	       A = gsl_vector_get(x_fit,0), B=gsl_vector_get(x_fit,1),
	       T = gsl_vector_get(x_fit,2);
	for(double t=t_0;t<=t_max;t+=delta_t){
		fprintf(data,"%lg\t%lg\n",t,A*exp(-t/T)+B);
	}
	fclose(data);

	// Free spcae
	gsl_vector_free(v);
	gsl_vector_free(x_fit);
	return 0;
}
