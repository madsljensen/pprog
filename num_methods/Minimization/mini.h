#ifndef HAVE_MINI_H
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<assert.h>
#include<math.h>

void vector_print(const gsl_vector* V);
void matrix_print(const gsl_matrix* M);
void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);
void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B);

int qnewt(double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H), 
		gsl_vector* p, double dx, double eps);
void grad(double f(gsl_vector* x), gsl_vector* x, gsl_vector* df, double dx);
int newt_broyden_num_grad(double f(gsl_vector* x), gsl_vector* x, double dx, double eps);

#define HAVE_MINI_h
#endif
