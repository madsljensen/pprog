#include"least.h"

double funs(int i, double x){
        switch(i){
                case 0: return log(x);  break;
                case 1: return 1.0;     break;
                case 2: return x;       break;  
                default: {fprintf(stderr,"funs: wrong i:%d\n",i); return NAN;}
         }
}

int main(){
	int m=3;
	
	/* Define data and size of double*/
	double x[]  = {0.1,1.33,2.55,3.78,5,6.22,7.45,8.68,9.9};
	double y[]  = {-15.3,0.3,2.45,2.75,2.27,1.35,0.157,-1.23,-2.75};
	double dy[] = {1.04,0.594,0.983,0.998,1.11,0.398,0.535,0.968,0.478};
	
	int n = sizeof(x)/sizeof(x[0]);

	/*Print data*/
	for(int i=0;i<n;i++){
		printf("%lg %lg %lg\n",x[i],y[i],dy[i]);
	}
	printf("\n\n");

	/* Allocate matrices and vectors*/
	gsl_vector* Vx = gsl_vector_alloc(n);
	gsl_vector* Vy = gsl_vector_alloc(n);
	gsl_vector* Vdy = gsl_vector_alloc(n);
	gsl_vector* c = gsl_vector_alloc(m);
	gsl_vector* dc = gsl_vector_alloc(m);
	gsl_matrix* S = gsl_matrix_alloc(m,m);
	
	/* FIll the vectors with the data*/
	for(int i=0;i<n;i++){
		gsl_vector_set(Vx,i,x[i]);
		gsl_vector_set(Vy,i,y[i]);
		gsl_vector_set(Vdy,i,dy[i]);
	}

	/* Do least square fit*/
	least_square_fit(m,funs,Vx,Vy,Vdy,c,S);
		for(int i=0;i<m;i++){
		double Sii = gsl_matrix_get(S,i,i);
		gsl_vector_set(dc,i,sqrt(fabs(Sii)));
	}
	/* Create functions that calculates the function value in a given point*/
	double fit(double x){
		double s=0;
		for(int i =0;i<m;i++){
			s+=gsl_vector_get(c,i)*funs(i,x);
		}
	return s;
	}
	
	// Calculate error on fit
	double fit_err(double x, gsl_matrix* S){
		double s = 0;
		for(int i=0;i<m;i++){
			for(int j=0;j<m;j++){
				s += funs(i,x)*funs(j,x)*gsl_matrix_get(S,i,j);
			}
		}
		return sqrt(s);
	}


	/* Print fits */
	double z, step_size = (x[n-1]-x[0])/100;
	z = x[0];
	do{
		printf("%lg %lg %lg %lg\n",z,fit(z),fit(z)-fit_err(z,S),fit(z)+fit_err(z,S));
		z+=step_size;
	}while(z<x[n-1]+step_size);


	/* Free stuff*/
	gsl_vector_free(Vx);
	gsl_vector_free(Vy);
	gsl_vector_free(Vdy);
	gsl_vector_free(c);
	gsl_vector_free(dc);
	gsl_matrix_free(S);
	return 0;
}
