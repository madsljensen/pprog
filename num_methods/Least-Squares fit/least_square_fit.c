#include"least.h"

void least_square_fit(int m, double funs(int i, double x), gsl_vector* x, gsl_vector* y,
		gsl_vector* dy, gsl_vector* c, gsl_matrix* S){
	
	int n = x->size;
	
	/* Allocate matrices and vector */
	gsl_matrix* A = gsl_matrix_alloc(n,m);
	gsl_matrix* R = gsl_matrix_alloc(m,m);
	gsl_matrix* invR = gsl_matrix_alloc(m,m);
	gsl_matrix* I = gsl_matrix_alloc(m,m);
	gsl_vector* b = gsl_vector_alloc(n);
	
	/* load data */
	for(int i=0;i<n;i++){
		double x_i = gsl_vector_get(x,i);
		double y_i = gsl_vector_get(y,i);
		double dy_i = gsl_vector_get(dy,i);
		gsl_vector_set(b,i,y_i/dy_i);

		for(int j=0;j<m;j++){
			gsl_matrix_set(A,i,j,funs(j,x_i)/dy_i);
		}
	}
	qr_gs_decomp(A,R);
	qr_gs_solve(A,R,b,c);
	
	gsl_matrix_set_identity(I);
	qr_gs_inverse(I,R,invR);

	gsl_blas_dgemm(CblasNoTrans,CblasTrans,1,invR,invR,0,S);
	
	/* free stuff */
	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_matrix_free(invR);
	gsl_matrix_free(I);
	gsl_vector_free(b);
}
