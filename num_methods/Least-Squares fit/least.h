#ifndef HAVE_LEAST_H
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<assert.h>
#include<math.h>

void vector_print(const gsl_vector* V);
void matrix_print(const gsl_matrix* M);
void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);
void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B);
void least_square_fit(int m, double funs(int i, double x), gsl_vector* x, gsl_vector* y,
	       	gsl_vector* dy, gsl_vector* c, gsl_matrix* S);
void matrix_inverse(gsl_matrix* M);

#define HAVE_LEAST_h
#endif
