#include"root.h"
#include<gsl/gsl_multiroots.h>

void f(gsl_vector* x, gsl_vector* fx){
	double A=10000;
	double X = gsl_vector_get(x,0);
	double Y = gsl_vector_get(x,1);
	gsl_vector_set(fx,0,A*Y*X-1.0);
	gsl_vector_set(fx,1,exp(-X)+exp(-Y)-1.0-1.0/A);
}

void rosenbrock(gsl_vector* x, gsl_vector* fx){
	double X = gsl_vector_get(x,0);
	double Y = gsl_vector_get(x,1);
	gsl_vector_set(fx,0,2*(X-1)-400*X*(Y-X*X));
	gsl_vector_set(fx,1,200*(Y-X*X));
}

void himmelblau(gsl_vector* x, gsl_vector* fx){
	double X = gsl_vector_get(x,0);
	double Y = gsl_vector_get(x,1);
	gsl_vector_set(fx, 0, 2.0*(2.0*X*(X*X + Y -11.0) + X + Y*Y - 7.0));
	gsl_vector_set(fx, 1, 2.0*(X*X + 2.0*Y*(X + Y*Y - 7.0) + Y - 11.0));
	
}

void f_jac(gsl_vector* x, gsl_vector* fx, gsl_matrix* J){
	double A = 10000;
	double X = gsl_vector_get(x,0);
	double Y = gsl_vector_get(x,1);
	gsl_vector_set(fx,1,A*Y*X-1.0);
	gsl_vector_set(fx,1,exp(-X)+exp(-Y)-1.0-1.0/A);
	gsl_matrix_set(J,0,0,A*Y);
	gsl_matrix_set(J,0,1,A*X);
	gsl_matrix_set(J,1,0,-1.0*exp(-X));
	gsl_matrix_set(J,1,1,-1.0*exp(-Y));
}

void rosenbrock_jac(gsl_vector* x, gsl_vector* fx, gsl_matrix* J){
        double X = gsl_vector_get(x,0);
	double Y = gsl_vector_get(x,1);
        gsl_vector_set(fx,0,2.0*(X-1)-400.0*X*(Y-X*X));
        gsl_vector_set(fx,1,200.0*(Y-X*X));
	gsl_matrix_set(J, 0, 0, 2.0*(600.0*X*X - 200.0*Y + 1.0));
	gsl_matrix_set(J, 0, 1, -400.0*X);
	gsl_matrix_set(J, 1, 0, -400.0*X);
	gsl_matrix_set(J, 1, 1, 200.0);	
}

void himmelblau_jac(gsl_vector* x, gsl_vector* fx, gsl_matrix* J){
        double X = gsl_vector_get(x,0);
        double Y = gsl_vector_get(x,1);
	gsl_vector_set(fx, 0, 2.0*(2.0*X*(X*X + Y -11.0) + X + Y*Y - 7.0));
	gsl_vector_set(fx, 1, 2.0*(X*X + 2.0*Y*(X + Y*Y - 7.0) + Y - 11.0));
	
	gsl_matrix_set(J, 0, 0, 2*(4.0*X*X + 2.0*(X*X +Y -11.0) +1.0));
	gsl_matrix_set(J, 0, 1, 2.0*(2.0*X+2.0*Y));
	gsl_matrix_set(J, 1, 0, 2.0*(2.0*X+2.0*Y));
	gsl_matrix_set(J, 1, 1, 2.0*(4.0*Y*Y + 2.0*(Y*Y + X - 7.0) +1.0));
}


int f_gsl_solve(const gsl_vector* x, void* params, gsl_vector* fx){
        double A = 10000;
        double X = gsl_vector_get(x,0);
        double Y = gsl_vector_get(x,1);
        	gsl_vector_set(fx, 0, 2.0*(2.0*X*(X*X + Y -11.0) + X + Y*Y - 7.0));
			gsl_vector_set(fx, 1, 2.0*(X*X + 2.0*Y*(X + Y*Y - 7.0) + Y - 11.0));
	
	//gsl_vector_set(fx,0,2.0*(X-1)-400.0*X*(Y-X*X));
	//gsl_vector_set(fx,1,200.0*(Y-X*X));

	
	//gsl_vector_set(fx,1,A*Y*X-1.0);
        //gsl_vector_set(fx,1,exp(-X)+exp(-Y)-1.0-1.0/A);
	return GSL_SUCCESS;
}

int main(){
	
	/* Allocate space*/
	gsl_vector* x = gsl_vector_alloc(2);
	gsl_vector* fx = gsl_vector_alloc(2);

	double step = 1e-6, eps = 1e-12;

// _____TASK 1_____
	printf("_____TASK 1_____\n");
	

	// System of equations
	gsl_vector_set(x,0,1.0/10000);
	gsl_vector_set(x,1,1);
	printf("Root of system of equations given the starting points\n");
	vector_print(x);
	f(x,fx);
	int iter =  newton(f,x,step,eps); int iter_f = iter;
	printf("is\n");
	vector_print(x);
	printf("Number of iterations = %i\n\n",iter);
	
	// Rosenbrock's vally function
	gsl_vector_set(x,0,2.0);
	gsl_vector_set(x,1,2.0);
	printf("Minimum of Rosenbrock's equation given the starting points\n");
	vector_print(x);
	rosenbrock(x,fx);
	iter = newton(rosenbrock,x,step,eps); int iter_ros = iter;
	printf("is\n");
	vector_print(x);
	printf("Number of iterations = %i\n\n",iter);
	
	// Himmelblau's function
        gsl_vector_set(x,0,4.0);
        gsl_vector_set(x,1,4.0);
        printf("Minimum of Himmelblaus's equation given the starting points\n");
        vector_print(x);
        himmelblau(x,fx);
        iter = newton(himmelblau,x,step,eps); int iter_him = iter;
        printf("is\n");
        vector_print(x);
        printf("Number of iterations = %i\n\n",iter);

//_____TASK 2_____
	printf("_____TASK 2____\n");
	
	// System of equations with analytical jacobian
        gsl_vector_set(x,0,1.0/10000);
        gsl_vector_set(x,1,1.0);
        printf("Root of system of equation with analytical jacobial given the starting points\n");
        vector_print(x);
        iter = newton_jac(f_jac,x,step,eps);
        printf("is\n");
        vector_print(x);
        printf("Number of iterations = %i compared to the numerical %i\n",iter,iter_f);
        
	// Rosenbrock with analytical jacobian
	gsl_vector_set(x,0,2.0);
        gsl_vector_set(x,1,2.0);
        printf("Minimum of Rosenbrock function with analytical jacobial given the starting points\n");
        vector_print(x);
        iter = newton_jac(rosenbrock_jac,x,step,eps);
        printf("is\n");
        vector_print(x);
        printf("Number of iterations = %i compared to the numerical %i\n\n",iter,iter_ros);
	
	// Himmelblau with analytical jacobian
        gsl_vector_set(x,0,4.0);
        gsl_vector_set(x,1,4.0);
        printf("Minimum of Himmalblau function with analytical jacobial given the starting points\n");
        vector_print(x);
        iter = newton_jac(himmelblau_jac,x,step,eps);
        printf("is\n");
        vector_print(x);
        printf("Number of iterations = %i compared to the numerical %i\n\n",iter,iter_him);

	
	// Calculate roots with gsl rutine
	const gsl_multiroot_fsolver_type* T = gsl_multiroot_fsolver_dnewton;
	gsl_multiroot_fsolver* S = gsl_multiroot_fsolver_alloc(T,2);

	gsl_multiroot_function F;
	F.f = f_gsl_solve;
	F.n = 2;
	F.params = NULL;

        gsl_vector_set(x,0,2.0);
        gsl_vector_set(x,0,2.0);
	gsl_multiroot_fsolver_set(S,&F,x);

	int flag;
	iter = 0;
	do{
		iter++;
		gsl_multiroot_fsolver_iterate(S);
		flag = gsl_multiroot_test_residual(S->f,eps);
	}while(flag!=GSL_SUCCESS);
	
	printf("Using GSL routine, one finds the solutions for Himmelblau's function\n");
	vector_print(S->x);
	printf("in %i iterations\n",iter);

	/* Free space*/
	gsl_multiroot_fsolver_free(S);
	gsl_vector_free(x);
	gsl_vector_free(fx);
	return 0;
}
