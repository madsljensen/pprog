#include"root.h"

int newton(void f(gsl_vector* x, gsl_vector* fx), gsl_vector* x_0, double dx, double eps){
	int n = x_0->size;
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* x_k = gsl_vector_alloc(n);
	gsl_vector* df_k = gsl_vector_alloc(n);
	gsl_vector* f_delta_x = gsl_vector_alloc(n);
	gsl_vector* delta_x = gsl_vector_alloc(n);
	
	double lambda, fx_norm, f_delta_x_norm;
	int iter = 0;

	do{
		iter++;
		f(x_0,fx);
		fx_norm = gsl_blas_dnrm2(fx);
		gsl_vector_memcpy(x_k,x_0);

		// Calculate Jacobian
		for(int k=0;k<n;k++){
			gsl_vector_set(x_k,k,gsl_vector_get(x_0,k)+dx);
			f(x_k,df_k);
			gsl_vector_sub(df_k,fx);
			gsl_vector_scale(df_k,1.0/dx);
			gsl_matrix_set_col(J,k,df_k);
			gsl_vector_set(x_k,k,gsl_vector_get(x_0,k));
		}

		// Solve J*deltax = -f(x)
		qr_gs_decomp(J,R);
		gsl_vector_scale(fx,-1.0);
		qr_gs_solve(J,R,fx,delta_x);
		gsl_vector_scale(fx,-1.0);

		// Backtracking
		lambda = 1.0;
		gsl_vector_add(x_0,delta_x);

		f(x_0,f_delta_x);
		f_delta_x_norm = gsl_blas_dnrm2(f_delta_x);

		while(f_delta_x_norm > (1-lambda/2.0)*fx_norm && lambda > 1.0/64.0){
			iter++;
			lambda/=2.0;
			gsl_vector_scale(delta_x,lambda);
			gsl_vector_add(x_0,delta_x);
			f(x_0,f_delta_x);
			f_delta_x_norm = gsl_blas_dnrm2(f_delta_x);
		}
	}while(fx_norm > eps && iter < 1e6);

	
	// Free space
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(fx);
	gsl_vector_free(x_k);
	gsl_vector_free(df_k);
	gsl_vector_free(delta_x);
	gsl_vector_free(f_delta_x);
	return iter;
}

int newton_jac(void f(gsl_vector* x, gsl_vector* fx, gsl_matrix* J), gsl_vector* x_0, double dx, double eps){
	int n = x_0->size;
	
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* df_k = gsl_vector_alloc(n);
	gsl_vector* delta_x = gsl_vector_alloc(n);
	gsl_vector* f_delta_x = gsl_vector_alloc(n);
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);

	double lambda, fx_norm, f_delta_x_norm;
	int iter = 0;

	do{
		iter++;
		f(x_0,fx,J);
		fx_norm = gsl_blas_dnrm2(fx);

		qr_gs_decomp(J,R);
		gsl_vector_scale(fx,-1.0);
		qr_gs_solve(J,R,fx,delta_x);
		gsl_vector_scale(fx,-1.0);

		lambda = 1.0;
		gsl_vector_add(x_0,delta_x);

		f(x_0,f_delta_x,J);
		f_delta_x_norm = gsl_blas_dnrm2(f_delta_x);

		while(f_delta_x_norm > (1.0-lambda/2.0)*fx_norm && lambda > 1.0/64.0){
			iter++;
			lambda /= 2.0;
			gsl_vector_scale(delta_x,lambda);
			gsl_vector_add(x_0,delta_x);
			f(x_0,f_delta_x,J);
			f_delta_x_norm = gsl_blas_dnrm2(f_delta_x);
		}
	}while(fx_norm > eps && iter < 1e6);

	gsl_vector_free(fx);
	gsl_vector_free(df_k);
	gsl_vector_free(delta_x);
	gsl_vector_free(f_delta_x);
	gsl_matrix_free(R);
	gsl_matrix_free(J);
	return iter;
}
