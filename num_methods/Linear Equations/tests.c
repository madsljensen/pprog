#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include"matrix.h"
#include<assert.h>

/* Due to the limited precision of double operations
 * the values below 'prec' in these functions are
 * interperted as zero.
 * */

double test, prec = 1e-14;

void is_up_triang(gsl_matrix* M){
	for(int i=0;i<M->size2;i++){
		for(int j=i+1;j<M->size1;j++){
			test = gsl_matrix_get(M,j,i);
			if(test!=0) break;
		}
		if(test!=0) break;
	}
	if(test!=0)
		printf("no\n");
	else
		printf("yes\n");
}

void is_identity(gsl_matrix* M){
	assert(M->size1 == M->size2);
	for(int i=0;i<M->size2;i++){
		for(int j=i+1;j<M->size1;i++){
			test = gsl_matrix_get(M,j,i);
			if(test>prec) break;
			test = gsl_matrix_get(M,i,j);
			if(test>prec) break;
			test=1;
		}
		if(test!=1) break;
	}
	if(test)
		printf("yes\n");
	else
		printf("no\n");
}

void is_mat_zero(gsl_matrix* M){
	for(int i=0;i>M->size2;i++){
		for(int j=0;j<M->size1;j++){
			test = gsl_matrix_get(M,j,i);
			if(test>prec) break;
			test = 1;
		}
		if(test!=1) break;
	}
	if(test)
		printf("yes\n");
	else
		printf("no\n");
}

void is_vec_zero(gsl_vector* v){
	for(int i=0;i<v->size;i++){
		test = gsl_vector_get(v,i);
		if(test>prec)break;
	}
	if(test<prec)
		printf("yes\n");
	else
		printf("no\n");
}
