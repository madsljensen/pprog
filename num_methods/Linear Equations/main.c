#include<stdio.h>
#include<stdlib.h>
#include"matrix.h"
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>

int main(){
printf("In the following functions are used to investigate properties and prints either -yes- or -no- depending on the argument fulfilling the needed criterion.\n");
printf("These functions are found in tests.c.\n\n");
//________TASK 1 ________
	/* Generate matrix */
	int n=5, m=4;
	
	gsl_matrix* A = gsl_matrix_alloc(n,m);
	gsl_matrix* A_copy = gsl_matrix_alloc(n,m);
	gsl_matrix* R = gsl_matrix_calloc(m,m);
	
	for(int i=0;i<A->size2;i++){
		for(int j=0;j<A->size1;j++){
			gsl_matrix_set(A,j,i,rand()%10);
		}
	}
	gsl_matrix_memcpy(A_copy,A);
	
	/* Print matrix in nice representation */
	printf("_____ Task 1.1 _____\n");
	printf("A\(%ix%i\) = \n",n,m);
	matrix_print(A);
	
	/* Make decomposition */	
	qr_gs_decomp(A,R);
	/* Print R and Q*/
	printf("R =\n");
	matrix_print(R);
	printf("Q =\n");
	matrix_print(A);
	/* Check R is upper triangular*/
	printf("R upper triangular?\n");
	is_up_triang(R);	

	/* Check that Q^TQ = identity */
	gsl_matrix* result = gsl_matrix_alloc(m,m);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,A,A,0.0,result);
	printf("Q^TQ ?= identity\n");
	is_identity(result);
	
	/* Check that QR=A */
	gsl_matrix* A_cal = gsl_matrix_alloc(n,m);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,R,0.0,A_cal);
	printf("QR ?= A\n");
	gsl_matrix_sub(A_cal,A_copy);
	is_mat_zero(A_cal);

	/* Generate random square matrix A_2, R_2, b and x*/
	gsl_matrix* A_2 = gsl_matrix_alloc(m,m);
	gsl_matrix* A_2_copy = gsl_matrix_alloc(m,m);
	gsl_matrix* R_2 = gsl_matrix_calloc(m,m);
	gsl_vector* b = gsl_vector_alloc(m);
	gsl_vector* b_copy = gsl_vector_alloc(m);
	gsl_vector* x = gsl_vector_alloc(m);
	
	for(int i=0;i<A_2->size2;i++){
		gsl_vector_set(b,i,rand()%10);
		for(int j=0;j<A_2->size1;j++)
			gsl_matrix_set(A_2,j,i,rand()%10);
	}
	gsl_matrix_memcpy(A_2_copy,A_2);
	gsl_vector_memcpy(b_copy,b);

	printf("\n_____ Task 1.2 _____\n");
	printf("A\(%ix%i\) = \n",m,m);
	matrix_print(A_2);
	printf("b = \n");
	vector_print(b);
	
	/* Solve system */
	qr_gs_decomp(A_2,R_2);
	qr_gs_solve(A_2,R_2,b,x);
	printf("x = \n");
	vector_print(x);

	/* Check solution */
	printf("Ax ?= b\n");
	gsl_blas_dgemv(CblasNoTrans,1.0,A_2_copy,x,0,b);
	gsl_vector_sub(b,b_copy);
	is_vec_zero(b);

// ________TASK 2_________
	printf("\n_____ Task 2 _____\n");
	
	/* Generate matrices A,B and R */
	gsl_matrix* A_3 = gsl_matrix_alloc(m,m);
	gsl_matrix* A_3_copy = gsl_matrix_alloc(m,m);
	gsl_matrix* B = gsl_matrix_alloc(m,m);
	gsl_matrix* R_3 = gsl_matrix_alloc(m,m);
	gsl_matrix* result_3 = gsl_matrix_alloc(m,m);

	for(int i=0;i<A_3->size1;i++){
		for(int j=0;j<A_3->size2;j++)
			gsl_matrix_set(A_3,i,j,rand()%10);
	}
	gsl_matrix_memcpy(A_3_copy,A_3);

	printf("A\(%ix%i\) = \n",m,m);
	matrix_print(A_3);
	
	/* Decompose matrix and find inverse */
	qr_gs_decomp(A_3,R_3);
	qr_gs_inverse(A_3,R_3,B);
	
	/* Print B*/
	printf("B =\n");
	matrix_print(B);

	/* Check AB = indentit */	
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A_3_copy,B,0,result_3);
	printf("AB ?=identity\n");
	is_identity(result_3);	

	
//______Free A LOT of stuff_______
	gsl_matrix_free(A);
	gsl_matrix_free(A_copy);
	gsl_matrix_free(R);
	gsl_matrix_free(result);
	gsl_matrix_free(A_cal);
	gsl_matrix_free(A_2);
	gsl_matrix_free(A_2_copy);
	gsl_matrix_free(R_2);
	gsl_vector_free(b);
	gsl_vector_free(b_copy);
	gsl_vector_free(x);
	gsl_matrix_free(A_3);
	gsl_matrix_free(B);
	gsl_matrix_free(R_3);
	gsl_matrix_free(A_3_copy);
	gsl_matrix_free(result_3);

	return 0;
}



