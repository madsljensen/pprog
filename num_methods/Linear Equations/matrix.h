#ifndef HAVE_MATRIX_H
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);
void matrix_print(const gsl_matrix* m);
void vector_print(const gsl_vector* v);
void qr_gs_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);

void is_up_triang(gsl_matrix* M);
void is_identity(gsl_matrix* M);
void is_mat_zero(gsl_matrix* M);
void is_vec_zero(gsl_vector* v);

#define HAVE_MATRIX_H
#endif
