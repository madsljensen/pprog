#include"adap.h"
#include<omp.h>

int main(){

	int calls = 0;
	double eps = 1e-6, acc = 1e-6, result;

	// Define functions
	double f1(double x){
		calls++;
		return sqrt(x);
	}

	double f2(double x){
		calls++;
		return 1.0/sqrt(x);
	}

	double f3(double x){
		calls++;
		return log(x)/sqrt(x);
	}

	double f4(double x){
		calls++;
		return 4*sqrt(1-(1.0-x)*(1.0-x));
	}

	double gauss(double x){
		calls++;
		return exp(-x*x/2);
	}

	double gauss_gsl(double x, void* p){
		calls++;
		return exp(-x*x/2);
	}
#pragma omp parallel sections
	{
#pragma omp section
		{	
	//_____TASK 1_____
	FILE* data1 = fopen("out1.txt","w+");
	fprintf(data1,"_____TASK 1_____\n\n");
	
	fprintf(data1,"The following integrals is done with acc = %lg and eps = %lg.\n\n",acc,eps);

	// Sqrt(x) from 0 to 1
	result = adapt(f1,0,1,acc,eps);
	fprintf(data1,"sqrt(x) integrated from 0 to 1 is equal to %.20lg in %i calls\n",result,calls);
	
	// 1/sqrt(x) from 0 to 1
	calls = 0;
        result = adapt(f2,0,1,acc,eps);
	fprintf(data1,"1/sqrt(x) integrated from 0 to 1 is equal to %lg in %i calls\n",result,calls);

	// log(x)/sqrt(x) from 0 to 1
	calls = 0;
        result = adapt(f3,0,1,acc,eps);
	fprintf(data1,"ln(x)/sqrt(x) integrated from 0 to 1 is equal to %lg in %i calls\n",result,calls);

	//4*sqrt(1-(1-x)^2 from 0 to 1
        calls = 0;
        result = adapt(f4,0,1,acc,eps);
        fprintf(data1,"4*sqrt(1-(1-x)^2) integrated from 0 to 1 is equal to %.20lg in %i calls\n",result,calls);
	fclose(data1);
}
#pragma omp section
{
	//_____TASK 2_____
	FILE* data2 = fopen("out2.txt","w+");
	fprintf(data2,"\n____TASK 2_____\n\n");		
		
	// Gauss in different intervals
	fprintf(data2,"Gaussian function integrated from:\n");
	
	calls=0;
	result = adapt(gauss,-INFINITY,INFINITY,acc,eps);
	fprintf(data2,"-inf to inf is equal to %.20lg in %i calls\n",result,calls);
	
        calls=0;
        result = adapt(gauss,-INFINITY,0,acc,eps);
        fprintf(data2,"-inf to 0 is equal to %.20lg in %i calls\n",result,calls);

        calls=0;
        result = adapt(gauss,0,INFINITY,acc,eps);
        fprintf(data2,"0 to inf is equal to %.20lg in %i calls\n",result,calls);
	
	// GSL integration
	gsl_function F;
	F.function = &gauss_gsl;
	F.params = NULL;

	int limit = 1e6;
	gsl_integration_workspace* ws = gsl_integration_workspace_alloc(limit);
	double err;
	int flag = gsl_integration_qagi(&F,eps,acc,limit,ws,&result,&err);

	fprintf(data2,"The full integral of this even function is %lg with the GSL routine, so the integrator works just fine.\n",result);
	
	gsl_integration_workspace_free(ws);
	fclose(data2);
}
#pragma omp section 
{
	//_____TASK 3_____
	FILE* data3 = fopen("out3.txt","w+");
	fprintf(data3,"\n_____TASK 3_____\n\n");
	
	fprintf(data3,"The following integrals is calculated with the Clenshaw-Curtis transformation with eps = %lg and acc = %lg\n\n",eps,acc);

	// Clenshaw-Curtis routine on the previously defined functions with divergiencies
	calls = 0;
        result = clenshaw_curtis(f2,0,1,acc,eps);
        fprintf(data3,"1/sqrt(x) integrated from 0 to 1 is equal to %lg in %i calls\n",result,calls);

	calls = 0;
        result = clenshaw_curtis(f4,0,1,acc,eps);
        fprintf(data3,"4*sqrt(1-(1-x)^2) integrated from 0 to 1 is equal to %lg in %i calls\n",result,calls);

	fprintf(data3,"The Clenshaw-Curtis transformations reduces the number of function calls significantly compared to the previous integrations when divergencies are present, but not otherwise.\n");
	fclose(data3);
}
}
	return 0;
}
