#ifndef HAVE_ADAP_H
#include<stdio.h>
#include<assert.h>
#include<math.h>
#include<gsl/gsl_integration.h>

double adapt(double f(double), double a, double b, double acc, double eps);
double clenshaw_curtis(double f(double), double a, double b, double acc, double eps);

#define HAVE_ADAP_h
#endif
