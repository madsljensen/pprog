#include"eigen.h"
#define RND ((double)rand()/RAND_MAX)

int main(){
	int n = 5;

	printf("_____TASK 2_____\n");

	/* allocate matrices and vectors */
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* A1 = gsl_matrix_alloc(n,n);
	gsl_matrix* A2 = gsl_matrix_alloc(n,n);
	gsl_matrix* A3 = gsl_matrix_alloc(n,n);
	gsl_matrix* A4 = gsl_matrix_alloc(n,n);
	gsl_matrix* V = gsl_matrix_alloc(n,n);
	gsl_vector* e = gsl_vector_alloc(n);
	
	/* Creakte random symmetric matrix*/
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			double x = RND;
			gsl_matrix_set(A,i,j,x);
			gsl_matrix_set(A,j,i,x);
		}
	}
	gsl_matrix_memcpy(A1,A);
	gsl_matrix_memcpy(A2,A);
	gsl_matrix_memcpy(A3,A);
	gsl_matrix_memcpy(A4,A);


	printf("A(%ix%i) = \n",n,n);
	matrix_print(A);
	
	/* Diagonalization is preformed*/
	int N = 3;
	
	/* Lowest eigen value*/
	printf("\nThe eigenvalues found can be adjusted by letting phi = phi+theta, where theta can then be changed to involke a change of the sign in the trigonometric functions.\n");
	printf("The lowest eigenvalues is found for theta = 0.\n");
	printf("The first %i rows are eliminated, so that\n",N);
	
	int sweep_e_by_e_low = jacobi_e_by_e(A,e,V,3,0);

	matrix_print(A);
	printf("with eigenvalues\n");
	vector_print(e);
	printf("and eigenvectors\n");
	matrix_print(V);
	printf("Number of sweeps = %i\n\n",sweep_e_by_e_low);
	
	/* Heighest eigenvalue*/
	printf("The highest eigenvalues are found with a rotation angle of pi/2\n");
	printf("The first %i rows are eliminated, so that\n",N);
	
	int sweep_e_by_e_high = jacobi_e_by_e(A1,e,V,N,M_PI/2);

	matrix_print(A1);
	printf("with eigenvalues\n");
	vector_print(e);
	printf("and eigenvectors\n");
	matrix_print(V);
	printf("Number of sweeps = %i\n\n",sweep_e_by_e_high);

	/* Compare number of rotations in cyclic and value-by-value*/
	int cyc_sweep = jacobi(A2,e,V);
	int val_sweep = jacobi_e_by_e(A3,e,V,n,0.0);
	printf("The number of sweeps for diagonalization of a %ix%i matrix is %i for the cyclic method and %i for value-by-value method.\n",n,n,cyc_sweep,val_sweep);
	
	
	int val_sweep_single = jacobi_e_by_e(A4,e,V,1,0.0);
	printf("The number of sweeps needed to find the lowest eigenvalue in the value-by-value for a %ix%i matrix is %i. The number of sweeps needed by the cyclic method to diagonalize the same matrix is %i.\n",n,n,val_sweep_single,cyc_sweep);

	/* Free stuff */
	gsl_matrix_free(A);
	gsl_matrix_free(A1);
	gsl_matrix_free(A2);
	gsl_matrix_free(A3);
	gsl_matrix_free(A4);
	gsl_matrix_free(V);
	gsl_vector_free(e);
	return 0;
}
