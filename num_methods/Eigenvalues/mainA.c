#include"eigen.h"
#define RND ((double)rand()/RAND_MAX)

int main(int argc, char** argv){
printf("In the following functions are used to investigate properties and prints either -yes- or -no-depending on the argument fulfilling the needed criterion.\n");
printf("These functions are found in tests.c.\n\n");

//_____TASK 1_____	
	printf("_____ TASK 1_____\n");
	
	/* Allocate matrices*/
	int n;
	if(argc>1)n=atoi(argv[1]);
	else n =5;
	
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* A_copy = gsl_matrix_alloc(n,n);
	gsl_matrix* VTA = gsl_matrix_alloc(n,n);
	gsl_matrix* VTAV = gsl_matrix_alloc(n,n);
	gsl_matrix* V = gsl_matrix_alloc(n,n);
	gsl_vector* e = gsl_vector_alloc(n);

	/* Create random symmetric matrix*/
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			double x = RND;
			gsl_matrix_set(A,i,j,x);
			gsl_matrix_set(A,j,i,x);
		}
	}

	/* Print matrix */
	printf("A(%ix%i) = \n",n,n);
	matrix_print(A);
	
	/* Diagolanization */
	gsl_matrix_memcpy(A_copy,A);
	int rot = jacobi(A_copy,e,V);
	printf("number of sweeps = %i\n",rot);
	
	/* Print V*/
	printf("V =\n");
	matrix_print(V);

	/* Check is algorithm works correctly */
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,A,0.0,VTA);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,VTA,V,0.0,VTAV);
	
	printf("Is VTAV diagonal?\n");
	is_mat_diag(VTAV);	

	printf("Is VTAV's diagonal equal to eigenvalues?\n");
	is_diag_equal(VTAV,e);	
	
	printf("A plot is made to investigate the n^3 proportionality between the size of the matrix and the time taken to fully diagonalize it.\n");
	/* Free stuff */
	gsl_matrix_free(A);
	gsl_matrix_free(A_copy);
	gsl_matrix_free(VTA);
	gsl_matrix_free(VTAV);
	gsl_matrix_free(V);
	gsl_vector_free(e);
	
	return 0;
}


