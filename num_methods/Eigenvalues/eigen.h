#ifndef HAVE_EIGEN_H
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<math.h>
#include<stdio.h>
#include<gsl/gsl_blas.h>

int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);
int jacobi_e_by_e(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int N, double theta);
void matrix_print(const gsl_matrix* m);
void is_mat_diag(gsl_matrix* M);
void vector_print(const gsl_vector* V);
void is_diag_equal(gsl_matrix* M, gsl_vector* V);

#define HAVE_EIGEN_H
#endif


