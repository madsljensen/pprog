#include"eigen.h"

int jacobi_e_by_e(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int N, double theta){
	int n = A->size1, changed, sweeps = 0;
	gsl_vector* ee = gsl_vector_alloc(n);
	gsl_matrix* VV = gsl_matrix_alloc(n,n);
	gsl_matrix_set_identity(VV);
	int p, q;

	for(int i=0;i<n;i++){
		gsl_vector_set(ee,i,gsl_matrix_get(A,i,i));
	}
	for(p=0; p<N; p++){
		do{changed=0; sweeps++;
			for(q=p+1;q<n;q++){
				double app = gsl_vector_get(ee,p);
				double aqq = gsl_vector_get(ee,q);
				double apq = gsl_matrix_get(A,p,q);
				double phi = 1.0/2*atan2(2*apq,aqq-app)+theta;
				double s = sin(phi), c = cos(phi);
				double app1 = c*c*app-2*s*c*apq+s*s*aqq;
				double aqq1 = s*s*aqq+2*s*c*apq+c*c*aqq;

				if(app1!=app){
					changed = 1;
					gsl_vector_set(ee,p,app1);
					gsl_vector_set(ee,q,aqq1);
					gsl_matrix_set(A,p,q,0.0);

					for(int i=p+1;i<q;i++){
						double api = gsl_matrix_get(A,p,i);
						double aiq = gsl_matrix_get(A,i,q);
						gsl_matrix_set(A,p,i,c*api-s*aiq);
						gsl_matrix_set(A,i,q,s*api+c*aiq);
					}

					for(int i=q+1;i<n;i++){
						double api = gsl_matrix_get(A,p,i);
						double aqi = gsl_matrix_get(A,q,i);
						gsl_matrix_set(A,p,i,c*api-s*aqi);
						gsl_matrix_set(A,q,i,s*api+c*aqi);
					}
					
					for(int i=0;i<n;i++){
						double aip = gsl_matrix_get(VV,i,p);
						double aiq = gsl_matrix_get(VV,i,q);
						gsl_matrix_set(VV,i,p,c*aip-s*aiq);
						gsl_matrix_set(VV,i,q,s*aip+c*aiq);
					}
				}
				else if (app1==app){
					gsl_vector_set(e,p,app1);
					for(int i=0;i<n;i++){
						gsl_matrix_set(V,i,p,gsl_matrix_get(VV,i,p));
					}
				}
			}
		}while(changed!=0);
	}
	gsl_matrix_free(VV);
	gsl_vector_free(ee);
	return sweeps;
}
