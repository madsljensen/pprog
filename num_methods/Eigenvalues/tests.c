#include"eigen.h"

/* Due to the limited precision of double operations
 * the values below 'prec' in these functions are
 * interperted as zero.
 **/

double test, prec = 1e-8;

void matrix_print(const gsl_matrix* m){
        for(int j=0;j<m->size1;j++){
                for(int i=0;i<m->size2;i++){	
			printf("%8.3g ",gsl_matrix_get(m,j,i));
		}
		printf("\n");
       }
}

void vector_print(const gsl_vector* v){
        for(int j=0;j<v->size;j++){
                printf("%8.3g\n",gsl_vector_get(v,j));
        }
}



void is_mat_diag(gsl_matrix* M){
	test = 1;
	for(int i=0;i<M->size1;i++){
		for(int j=0;j<M->size2;j++){
			if(i!=j && fabs(gsl_matrix_get(M,i,j))>prec){
				printf("i=%i, j=%i\n",i,j);
				test = 0;
				break;
			}
	
		}
	}
		
	if(test)
              printf("yes\n");
        else
	      printf("no\n");
}

void is_diag_equal(gsl_matrix* M, gsl_vector* V){
	test = 1;
	for(int i=0;i<M->size1;i++){
		if(fabs(gsl_matrix_get(M,i,i)-gsl_vector_get(V,i))>prec){
			test=0;
			break;
		}
	}

	if(test)
		printf("yes\n");
	else
		printf("no\n");
}
