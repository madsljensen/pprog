#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int diff_func (double x, const double y[], double dydx[], void* params){
	dydx[0] = 2/sqrt(M_PI)*exp(-x*x);
	return GSL_SUCCESS;
}

double solve_diff(double x){
	gsl_odeiv2_system my_system;
	my_system.function = diff_func;
	my_system.jacobian = NULL;
	my_system.dimension = 1;
	my_system.params = NULL;

	double hstart=copysign(0.1,x);
	double acc = 1e-6;
	double eps = 1e-6;
	gsl_odeiv2_driver* driver = 
		gsl_odeiv2_driver_alloc_y_new
		(&my_system,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double t=0;
	double y[1] = {0};
	gsl_odeiv2_driver_apply(driver,&t,x,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int main(int argc, char** argv){
	if(argc==4){
		double a, b, dx;
		a = atof(argv[1]);
		b = atof(argv[2]);
		dx = atof(argv[3]);
		for(double x=a; x<=b; x+=dx){
			printf("%g\t %g\n",x,solve_diff(x));
		}
	}
	else{printf("Incorrect number of input arguments\n");}
	return 0;
}
