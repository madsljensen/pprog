#include<stdlib.h>
#include<stdio.h>

int equal();

int main(){
	int a =equal(5.0,4.0,0.0,0.0);
	printf("a=%i\n",a);
}

int equal(double a, double b, double tau, double epsilon){
	if(abs(a-b)<tau)
		return 1;
	else if(abs(a-b)/(abs(a)+abs(b))<epsilon/2)
		return 1;
	else
		return 0;
}
