#include<stdio.h>
#include<limits.h>

void one_i()
{
	int a_0 = INT_MAX;
	printf("Max int from limit.h: %i \n",a_0);

	int i=1;
	while(i+1>i) 
	{
		i++;
	}
	printf("Max int from 'while': %i \n",i);


	for (i = 0; i < i+1;)
       	{
		i++;
	}
	printf("Max int from 'for': %i \n",i);

	int k=1;
	do
		k++;
	while(k<k+1);
	printf("Max int from 'do-while': %i \n \n",k);
}	
