#include<stdio.h>
#include<float.h>

void one_iii()
{
	/* flaot.h values defined and printed*/
	double a_0=DBL_EPSILON;
	float b_0 = FLT_EPSILON;
	long double c_0 = LDBL_EPSILON;

	printf("From float.h   : eps_d = %g, eps_f = %f, eps_ld = %Lg \n",a_0,b_0,c_0);
	
	/* epsilon calculated by while loop for alle three types*/
	double a_1=1;
	while(1+a_1!=1)
	{
		a_1/=2;
	}
	a_1*= 2;

	float b_1=1;
	while(1+b_1!=1)
	{
		b_1/=2;
	}
	b_1*=2;;

	long double c_1=1;
	while(1+c_1!=1)
	{
		c_1/=2;
	}
	c_1*=2;

	printf("From 'while'   : eps_d = %g, eps_f = %f, eps_ld = %Lg \n",a_1,b_1,c_1);

	/* epsilon calculated at printed by for loop for all three types*/
	double a_2;
	for(a_2=1; 1+a_2!=1; a_2/=2){}
	a_2*=2;

	float b_2;
	for(b_2=1; 1+b_2!=1; b_2/=2){}
	b_2*=2;

	long double c_2;
	for(c_2=1; 1+c_2!=1; c_2/=2){}
	c_2*=2;

	printf("From 'for'     : eps_d = %g, eps_f = %f, eps_ld = %Lg \n",a_2,b_2,c_2);

	/* epsilon calculated and printed by do-while loop for all three types*/
	double a_3 =1;
	do
		a_3/=2;
	while(1+a_3!=1);
	a_3*=2;
	
	float b_3=1;
	do
		b_3/=2;
	while(1+b_3!=1);
	b_3*=2;
	
        long double c_3=1;
	do
		c_3/=2;
	while(1+c_3!=1);
	c_3*=2;


	printf("From 'do-while': eps_d = %g, eps_f = %f, eps_ld = %Lg \n \n",a_3,b_3,c_3);
}
