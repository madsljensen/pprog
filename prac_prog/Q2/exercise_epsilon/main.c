#include<stdio.h>
#include<limits.h>
#include<float.h>

void one_i();
void one_ii();
void one_iii();
void two_i();
void two_iv();

int main()
{
	printf("1.i \n");
	one_i();

	printf("1.ii \n");
	one_ii();

	printf("1.iii \n");
	one_iii();

	printf("2.i \n");
	two_i();

	printf("2.ii \n");
	printf("The diffenrence is du to the limited number of digits.\n \n");

	printf("2.iii \n");
	printf("This sum is divergent, so no.\n \n");

	printf("2.iv \n");
	two_iv();
	printf("As more digits are taken into account, the two sums equals the same.\n \n");
}	
