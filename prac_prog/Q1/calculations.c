#include<tgmath.h>
#include<stdio.h>
#include<complex.h>

int main(void)
{
	int a=tgamma(5);

	double b=j1(0.5);

		printf("gamma(5)=%i, j1(0.5)=%g\n",a,b);
	
	complex i = csqrt(-1);
	complex c = csqrt(-2);
	complex d = cexp(i);
	int e = cexp(i*M_PI);
	complex f = cpow(i,(M_E));

	printf("sqrt(-2)=%g+i%g, e^i=%g+i%g, e^(i*pi)=%i, i^e=%g+i%g\n",creal(c),cimag(c),creal(d),cimag(d),e,creal(f),cimag(f));
}
