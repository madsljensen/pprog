#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>

int rosenbrock(const gsl_vector* current_point, void* params, gsl_vector* f){
	double x = gsl_vector_get(current_point,0);
	double y = gsl_vector_get(current_point,1);
	double fx = -2*(1-x)-400*x*(y-x*x);
	double fy = 200*(y-x*x);
	gsl_vector_set(f,0,fx);
	gsl_vector_set(f,1,fy);
	return GSL_SUCCESS;
}

double rosenbrock_min(double * xmin, double* ymin, int* k_it){
	const gsl_multiroot_fsolver_type* T = gsl_multiroot_fsolver_hybrids;
	gsl_multiroot_fsolver* S = gsl_multiroot_fsolver_alloc(T,2);

	gsl_multiroot_function F;
	F.f = rosenbrock;
	F.n = 2;
	F.params = NULL;

	gsl_vector* start = gsl_vector_alloc(2);
	gsl_vector_set(start,0,10);
	gsl_vector_set(start,1,10);
	gsl_multiroot_fsolver_set(S,&F,start);

	int flag, k=0;
	do{
		k++;
		gsl_multiroot_fsolver_iterate(S);
		flag = gsl_multiroot_test_residual(S->f,1e-12);
	}
	while(flag==GSL_CONTINUE);

	*xmin = gsl_vector_get(S->x,0);
	*ymin = gsl_vector_get(S->x,1);
	*k_it = k;
	
	gsl_vector_free(start);
	gsl_multiroot_fsolver_free(S);
	return 0;
}
