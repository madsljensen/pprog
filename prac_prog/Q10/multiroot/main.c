#include<stdio.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include<assert.h>

double rosenbrock_min();
double F(double e, double r);

int aux(const gsl_vector* current_point, void* params, gsl_vector* f){
	double e = gsl_vector_get(current_point,0);
	assert(e<0);
	double rmax = *(double*)params;
	double fval = F(e,rmax);
	gsl_vector_set(f,0,fval);
	return GSL_SUCCESS;
}

int main(){
// Print the solution to the Rosenbrock equation
	double xmin, ymin;
	int k_it;
	rosenbrock_min(&xmin,&ymin,&k_it);
	printf("# The minimum of the Rosenbrock function is (x,y) = (%g,%g)\n",xmin,ymin);
	printf("# It took %i interations.\n\n",k_it);
// Solve and print the radial SE for Hydrogen
	double rmax = 8.0;
	
	const gsl_multiroot_fsolver_type* T = gsl_multiroot_fsolver_hybrids;
	gsl_multiroot_fsolver* S = gsl_multiroot_fsolver_alloc(T,1);

	gsl_multiroot_function fun;
	fun.f = aux;
	fun.n = 1;
	fun.params = (void*)&rmax;

	gsl_vector* start = gsl_vector_alloc(1);
	gsl_vector_set(start,0,-2.0);
	gsl_multiroot_fsolver_set(S,&fun,start);

	int flag;
	do{
		gsl_multiroot_fsolver_iterate(S);
		flag = gsl_multiroot_test_residual(S->f,1e-12);
	}
	while(flag==GSL_CONTINUE);

	double e_0 = gsl_vector_get(S->x,0);

	gsl_vector_free(start);
	gsl_multiroot_fsolver_free(S);
	
	for(double r = 1e-3;r<rmax;r+=0.1){
		printf("%g %g %g\n",r,F(e_0,r),r*exp(-r));
	}
	return 0;
}
