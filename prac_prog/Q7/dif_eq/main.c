#include<stdio.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_errno.h>

int diff_func (double t, const double y[], double f[], void *params_ptr) /* define diff. eq.*/
{
	(void)(t); 
	f[0] = y[0]*(1-y[0]);
	return GSL_SUCCESS;
}

int main(){
	int dim = 1; 			/* dimention of problem = number of equations*/

	double eps_abs = 1.e-8; 	/*absoloute error requested */
	double eps_rel = 1.e-10; 	/*relative error requested*/

	/* definition of the step routine*/
	const gsl_odeiv2_step_type *type_ptr = gsl_odeiv2_step_rkf45; 

	/* Allocating the stepper, control function and evolution function*/
	gsl_odeiv2_step *step_ptr = gsl_odeiv2_step_alloc (type_ptr,dim);
	gsl_odeiv2_control *control_ptr = gsl_odeiv2_control_y_new (eps_abs,eps_rel);
	gsl_odeiv2_evolve *evolve_ptr = gsl_odeiv2_evolve_alloc (dim);


	gsl_odeiv2_system my_system; 	/* System strycture*/

	double y[1]; 			/* current solution vector */
	
	double t, t_next; 		/*current and next independent cariable*/
	double tmin, tmax, delta_t; 	/* range of t and step size for outpu*/
	
	double h = 1.e-6; 		/* starting step size for ode solver */
	
	/* Load vales to system */
	my_system.function = diff_func; /* the right hand side og the equation*/
	my_system.jacobian = NULL;  /* The jacobian df[i]/dy[j] */
	my_system.dimension = dim; 	/* number of diff. eq. */
	
	tmin = 0; 			/* min t */
	tmax = 3;  			/* max t */
	delta_t = 0.01; 		/* t step size */

	y[0] = 0.5; 			/* initial y(0) value */

	t = tmin; 			/* initialize t */

	printf("%g %g %g\n",t,y[0],y[0]*(1-y[0])); 	/* print staraat values */
	
	/* step form tmin to tmax */	
	for(t_next = tmin+delta_t; t_next <=tmax;t_next += delta_t){
		while(t<t_next){
			gsl_odeiv2_evolve_apply(evolve_ptr,control_ptr,step_ptr,
					&my_system,&t,t_next,&h,y);
		}
		printf("%g %g %g\n",t,y[0],y[0]*(1-y[0]));
	}
	
	/* free stuff */
	gsl_odeiv2_evolve_free(evolve_ptr);
	gsl_odeiv2_control_free(control_ptr);
	gsl_odeiv2_step_free(step_ptr);
	return GSL_SUCCESS;	
}


