#include<stdio.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_errno.h>
#include<math.h>

int diff_func (double t, const double y[], double f[], void *params_ptr) /* define diff. eq.*/
{
	(void)(t);
       	double epsilon = *(double *) params_ptr; /* get parameter from params_ptr*/	
	f[0] = y[1];
	f[1] = 1-y[0]+epsilon*y[0]*y[0];
	return GSL_SUCCESS;
}

int main(){
	int dim = 2; 			/* dimention of problem = number of equations*/

	double eps_abs = 1.e-8; 	/*absoloute error requested */
	double eps_rel = 1.e-10; 	/*relative error requested*/

	/* definition of the step routine*/
	const gsl_odeiv2_step_type *type_ptr = gsl_odeiv2_step_rkf45; 

	/* Allocating the stepper, control function and evolution function*/
	gsl_odeiv2_step *step_ptr = gsl_odeiv2_step_alloc (type_ptr,dim);
	gsl_odeiv2_control *control_ptr = gsl_odeiv2_control_y_new (eps_abs,eps_rel);
	gsl_odeiv2_evolve *evolve_ptr = gsl_odeiv2_evolve_alloc (dim);


	gsl_odeiv2_system my_system; 	/* System strycture*/

	double y[2]; 			/* current solution vector */
	double epsilon_vals[3] = {0.0,0.0,0.01};

	double t, t_next; 		/*current and next independent cariable*/
	double tmin, tmax, delta_t; 	/* range of t and step size for outpu*/
	
	double h = 1.e-6; 		/* starting step size for ode solver */
	
	/* Load vales to system */
	my_system.function = diff_func; /* the right hand side og the equation*/
	my_system.jacobian = NULL;  /* The jacobian df[i]/dy[j] */
	my_system.dimension = dim; 	/* number of diff. eq. */

	tmin = 0.0; 			/* min t */
	double tmax_val[3] = {2*M_PI,2*M_PI,50*M_PI};  			/* max t */

	delta_t = 0.01; 		/* t step size */

	y[0] = 1;	/* initial y(0) value */

	double y_val[3] = {0.0,-0.5,-0.5};
	
	t = tmin; 			/* initialize t */


	/* step form tmin to tmax */	
	for(int i=0;i<3;i++){
		double epsilon = epsilon_vals[i];
		my_system.params = &epsilon;
		y[1] = y_val[i];
		tmax = tmax_val[i];
		
		t = tmin;
		for(t_next = tmin+delta_t; t_next <=tmax;t_next += delta_t){

			while(t<t_next){
				gsl_odeiv2_evolve_apply(evolve_ptr,control_ptr,step_ptr,
						&my_system,&t,t_next,&h,y);
				printf("%g %g\n",t,y[0]);

			}
		}
		printf("# epsilon=%g\n",epsilon);
		printf("\n\n");
	}

	/* free stuff */
	gsl_odeiv2_evolve_free(evolve_ptr);
	gsl_odeiv2_control_free(control_ptr);
	gsl_odeiv2_step_free(step_ptr);
	
	/* Print and free*/
//	matrix_print(M);
//	gsl_matrix_free(M);
	return GSL_SUCCESS;	
}


