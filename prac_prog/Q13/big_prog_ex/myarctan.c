#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include<assert.h>
#include<math.h>

int tan_eq(const gsl_vector* current_point, void* params, gsl_vector* f){
	double z = *(double*)params;
	double x = gsl_vector_get(current_point,0);
	gsl_vector_set(f,0,tan(x)-z);
	return GSL_SUCCESS;
}

double myarctan(double z){

	const gsl_multiroot_fsolver_type* T = gsl_multiroot_fsolver_hybrids;
	gsl_multiroot_fsolver* S = gsl_multiroot_fsolver_alloc(T,1);

	gsl_multiroot_function F;
	F.f = tan_eq;
	F.n = 1;
	F.params = (void*)&z;

	gsl_vector* start = gsl_vector_alloc(F.n);
	gsl_vector_set(start,0,1.0);
	gsl_multiroot_fsolver_set(S,&F,start);

	int flag;
	do{
		gsl_multiroot_fsolver_iterate(S);
		flag = gsl_multiroot_test_residual(S->f,1e-12);
	}
	while(flag==GSL_CONTINUE);

	double result = gsl_vector_get(S->x,0);
	gsl_vector_free(start);
	gsl_multiroot_fsolver_free(S);
	return result;
}
