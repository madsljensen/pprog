#include"nvec.h"
#include<stdio.h>
#include<stdlib.h>
#define RND (double)rand()/RAND_MAX

int main(){
	int n = 5;

// 	Test of nvector_alloc
	printf("\n main: testing nvector_alloc... \n");
	nvector *v = nvector_alloc(n);
	if (v == NULL)
		printf("test failed \n");
	else 
		printf("test passed \n");
	

//	Test of nvector_set and nvector_get
	printf("\n main: testing nvector_set and nvector_get... \n");
	double value = RND;
	int i = n/2;	
	nvector_set(v,i,value);
	double vi = nvector_get(v,i);
	if (double_equal(vi,value))
		printf("test passed\n");
	else
		printf("test failed \n");

// 	Test of nvector_dot_product
 	printf("\n main: test of nvector_dot_product... \n");
	nvector *q = nvector_alloc(3);
	
	for (int i=0;i<3;i++){nvector_set(q,i,1.0);}
	double sum = nvector_dot_product(q,q);
	if(double_equal(sum,3.0))
		printf("test passed\n");
	else
		printf("test failed\n");
	

	nvector_free(v);
	nvector_free(q);
	return 0;
}


