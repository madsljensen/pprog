#include<stdio.h>
#include"nvec.h"
#include<stdlib.h>
#include<math.h>

int double_equal(double a, double b){
	if (a==b)
		return 1;
	else
		return 0;
}

nvector* nvector_alloc(int n){
	nvector *v = malloc(n*sizeof(nvector));
	(*v).size = n;
	(*v).data = malloc(n*sizeof(double));
	if (v==NULL)
		fprintf(stderr,"error in nvector_alloc \n");
	return v;
}

void nvector_free (nvector* v){
	free(v->data);
	free(v);
}

void nvector_set (nvector* v, int i, double value){
	(*v).data[i] = value;
}

double nvector_get (nvector* v, int i){
	return (*v).data[i];
}

double nvector_dot_product(nvector* u, nvector* v){
	if ((*u).size==(*v).size){
		double q = 0.0;
		for (int i=0;i<3;i++){
			q += (*u).data[i]*(*v).data[i];
		} 
		return q;
	}
	else{
		printf("vectors are not same lenght \n");
	}
	return 0;
}
