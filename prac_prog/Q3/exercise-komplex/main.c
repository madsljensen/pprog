#include<stdio.h>
#include"komplex.h"
#define TINY 1e-6

int main(){
	komplex a={1,2}, b= {3,4};
	komplex_print("a=",a);
	komplex_print("b=",b);


	printf("Test of komplex_add \n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a+b should be",R);
	komplex_print("komplex_add(a,b) returns",r);
	printf("komplex_equal returns %i \n \n",komplex_equal(r,R));


	printf("Test of komplex_sub \n");
	komplex s = komplex_sub(b,a);
	komplex S = {2,2};
	komplex_print("b-a should be",S);
	komplex_print("komplex_sub(b,a) returns",s);

	printf("komples_equal returns %i \n \n",komplex_equal(s,S));


	printf("Test of komplex_mul \n");
	komplex M = {-5,10};
	komplex m = komplex_mul(a,b);
	komplex_print("a*b should equal",M);
	komplex_print("komplex_mul(a,b) returns",m);

	printf("komplex_equal returns %i \n \n",komplex_equal(m,M));


	printf("Test of komplex_div \n");
	komplex D = {-0.2,0.4};
	komplex d = komplex_div(a,b);
	komplex_print("a/b should return",D);
	komplex_print("komplex_div(a,b) returns",d);

	printf("komplex_equal(a,b) returns %i \n \n",komplex_equal(d,D));
}
