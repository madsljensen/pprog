#ifndef HAVE_KOMPLEX_H /*multible includes*/

struct komplex {double re; double im;};
typedef struct komplex komplex;

void komplex_print (char* s, komplex z); /* prints s and then komplex z */

komplex komplex_new (double x, double y);
komplex komplex_add (komplex, komplex b);
komplex komplex_sub (komplex a, komplex b);

/* Optional structs */
int komplex_equal (komplex a, komplex b);
komplex komplex_mul (komplex a, komplex b);
komplex komplex_div (komplex a, komplex b);


#define HAVE_KOMPLEX_H
#endif
