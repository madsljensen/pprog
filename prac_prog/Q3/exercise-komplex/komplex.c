#include<stdio.h>
#include"komplex.h"
#include<math.h>

void komplex_print(char *s, komplex a) {
	printf("%s (%g,%g)\n",s,a.re,a.im);
}

komplex komplex_new (double x, double y){
	komplex z = {x,y};
	return z;
}


komplex komplex_add (komplex a, komplex b){
	komplex result = {a.re +b.re, a.im + b.im};
	return result;
}

komplex komplex_sub (komplex a, komplex b){
	komplex result = {a.re - b.re, a.im - b.im};
	return result;
}

int komplex_equal(komplex a, komplex b){
	if(a.re==b.re && a.im==b.im)
		return 1;
	else 
		return 0;

}

komplex komplex_mul(komplex a, komplex b){
	komplex result={a.re*b.re-a.im*b.im, a.re*b.im+a.im*b.re};
	return result;
}

komplex komplex_div(komplex a, komplex b){
	komplex c = komplex_mul(a,b);
	komplex result={c.re/(pow(b.re,2)+pow(b.im,2)), c.im/(pow(b.re,2)+pow(b.im,2))};
	return result;
}
