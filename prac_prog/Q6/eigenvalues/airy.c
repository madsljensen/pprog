#include<gsl/gsl_sf_airy.h>
#include<stdio.h>
int main(){
	for(double i = -10;i<10;i+=0.2){
		printf("%g %g %g\n"
				,i
				,gsl_sf_airy_Ai(i,GSL_PREC_DOUBLE)
				,gsl_sf_airy_Bi(i,GSL_PREC_DOUBLE)
				);
	}
	return 0;
}

