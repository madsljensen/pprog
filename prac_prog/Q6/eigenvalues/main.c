#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_blas.h>

int main(){
// 	Space is allocated
	gsl_matrix * M = gsl_matrix_alloc (3,3);
	gsl_vector * V = gsl_vector_alloc(3);
	gsl_vector * x = gsl_vector_alloc(3);
	gsl_matrix * N = gsl_matrix_alloc(3,3);
	gsl_vector * b_res = gsl_vector_alloc(3);

//	Load the matrix and vector given ind the assignment
	FILE* g = fopen("matrix_data.txt","r");
	gsl_matrix_fscanf(g,M);
	fclose(g);
	
	FILE* d = fopen("vector_dat.txt","r");
	gsl_vector_fscanf(d,V);
	fclose(d);

// 	The system is solved
	gsl_matrix_memcpy(N,M); 		 /* The matrix is saved for later*/
	gsl_linalg_HH_solve(M,V,x);
	printf("The soulution is\n");
	gsl_vector_fprintf(stdout,x,"%g");

// 	The system is checked
	gsl_blas_dgemv(CblasNoTrans,1,N,x,0,b_res);
	printf("\n Making the product with this results in\n");
	gsl_vector_fprintf(stdout,b_res,"%g");
	
	return 0;
}
