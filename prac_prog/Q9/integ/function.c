#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double f(double x, void* params){
       double f = log(x)/sqrt(x);
       return f;
}


double function(){
	gsl_function F;
	F.function = &f;
	F.params = NULL;

	int limit = 100;
	gsl_integration_workspace* ws = gsl_integration_workspace_alloc(limit);

	double acc = 1e-6, eps = 1e-6, result, err;
	int flag = gsl_integration_qags(&F,0,1,eps,acc,limit,ws,&result,&err);

	gsl_integration_workspace_free(ws);

	if(flag!=GSL_SUCCESS) return NAN;
	else return result;
}
