#include<math.h>
#include<gsl/gsl_integration.h>
#include<stdio.h>
#include<gsl/gsl_errno.h>

double psi2(double x,void* params){
	double alpha  = *(double *) params;
	double psi2 =  exp(-alpha*x*x);
	return psi2;
}

double prod_psi(double alpha){
	gsl_function F;
	F.function = &psi2;
	F.params = &alpha;
	
	int limit = 100;
	gsl_integration_workspace* ws = gsl_integration_workspace_alloc(limit);

	double acc = 1e-7, eps = 1e-7, result, err;
	int flag = gsl_integration_qagi(&F,eps,acc,limit,ws,&result,&err);

	gsl_integration_workspace_free(ws);

	if(flag!=GSL_SUCCESS) return NAN;
	else return result;
}
