#include<stdio.h>
#include<math.h>

double function();
double prod_psi(double alpha);
double ham(double alpha);

int main(){
// The first integral is calculated
	printf("# def_integral1 = %g\n",function());

// Test of product function integration
	printf("# prod_psi(1) = %g\n",prod_psi(1.0));

// Test of hamilton integration
	printf("# ham(1) = %g\n",ham(1.0));

// Print values of E for inteval of alpha
	for(double x=0.01;x<10;x+=0.1){
		printf("%g %g\n",x,ham(x)/prod_psi(x));
	}
	return 0;
}
