#include<math.h>
#include<gsl/gsl_integration.h>
#include<stdio.h>
#include<gsl/gsl_errno.h>

double ham_int(double x,void* params){
	double alpha  = *(double*)params;
	double ham = (-alpha*alpha*x*x/2+alpha/2+x*x/2)*exp(-alpha*x*x);
	return ham;
}

double ham(double alpha){
	gsl_function F;
	F.function = &ham_int;
	F.params = &alpha;

	int limit = 100;
	gsl_integration_workspace* ws = gsl_integration_workspace_alloc(limit);

	double acc = 1e-6, eps = 1e-6, result, err;
	int flag = gsl_integration_qagi(&F,eps,acc,limit,ws,&result,&err);

	gsl_integration_workspace_free(ws);

	if(flag!=GSL_SUCCESS) return NAN;
	else return result;
}
